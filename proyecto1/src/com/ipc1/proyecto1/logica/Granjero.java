package com.ipc1.proyecto1.logica;

import java.util.Arrays;

public class Granjero {

    private String nombre;
    private String nickName;
    private int oro;
    private int vida;
    private Planta[] semillas;
    public static int alimentoConsumido=0;
    
    public Granjero(String nombre, String nickName, int oro, int vida) {
        this.nombre = nombre;
        this.nickName = nickName;
        this.oro = oro;
        this.vida = vida;
        this.semillas = new Planta[0];             
    }

    public String mostrarDatos() {
        return nickName;
    }

    public int getOro() {
        return this.oro;
    }

    public void ganarOro(int cantidadOro) {
        this.oro += cantidadOro;

    }

    public void gastarOro(int cantidadGastada) {
        this.oro -= cantidadGastada;

    }

    public void comer(int comida) {
        this.vida += comida;

    }

    public void restarVida(int vida) {
        this.vida -= vida;
    }

    public String morir() {
        if (this.vida <= 0) {
            return "El granjero se há quedado sin vida";
        }
        return null;
    }

    public int getVida() {
        return this.vida;
    }

    public void sembrarPlanta(Planta planta) {

    }

    public void adquirirSemilla(Planta semilla) {
        semillas = Arrays.copyOf(semillas, semillas.length + 1);
        semillas[semillas.length - 1] = semilla;
    }

    public Planta[] getSemillas() {
        return semillas;
    }

    public String getNickName() {
        return nickName;
    }

    public String getNombre() {
        return nombre;
    }
    
    

}
