package com.ipc1.proyecto1.logica;

public enum AlimentoAnimalOmnivoro {

    CARNE(15, 5),
    FRUTAS(25, 10),
    VERDURAS(35, 15);

    private final int alimentoOmnivoro;
    private final int precioAlimento;

    private AlimentoAnimalOmnivoro(int alimentoOmnivoro, int precioAlimento) {
        this.alimentoOmnivoro = alimentoOmnivoro;
        this.precioAlimento = precioAlimento;
    }

    public int getAlimentoOmnivoro() {
        return alimentoOmnivoro;
    }

    public int getPrecioAlimento() {
        return precioAlimento;
    }

}
