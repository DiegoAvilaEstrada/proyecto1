package com.ipc1.proyecto1.logica;

public abstract class Suelo {

    public final static int PRECIO_SUELO = 25;
    
    protected int numero;
    
    protected int fila;
    
    protected int columna;
    
    protected TipoSuelo tipoSuelo;

    public Suelo( int fila, int columna,int numero,TipoSuelo tipoSuelo) {
        this.numero = numero;
        this.fila = fila;
        this.columna = columna;
        this.tipoSuelo=tipoSuelo;
    }
     
    public abstract void producirComida();
    
    public abstract int limpiarTerreno();
    
    public int getNumero() {
        return numero;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public TipoSuelo getTipoSuelo() {
        return tipoSuelo;
    }
    
    
    
}
