package com.ipc1.proyecto1.logica;

import com.ipc1.proyecto1.gui.CasillaGUI;
import com.ipc1.proyecto1.gui.GranjaGUI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class Animal {
    
    private String nombreAnimal;
    private int vida;
    private double espacioOcupado;
    private int comidaDestace;
    private int comidaSinDestace;
    private int materiaPrimaConDestace;
    private int materiaPrimaSinDestace;
    private int precio;
    private TipoAnimal tipoAnimal;
    private int edad;
    private boolean fueDestazado;
    private final int edadFinal;

    //CONSTRUCTOR DE MI ANIMAL, IMPLEMENTACION DE LOS ATRIBUTOS QUE TENDRÁ CADA ANIMAL AL CREARLO
    public Animal(String nombreAnimal, int vida, double espacioOcupado, int comidaDestace, int comidaSinDestace, int materiaPrimaConDestace, int materiaPrimaSinDestace, int precio, TipoAnimal tipoAnimal, int edad) {
        this.nombreAnimal = nombreAnimal;
        this.vida = vida;
        this.espacioOcupado = espacioOcupado;
        this.comidaDestace = comidaDestace;
        this.comidaSinDestace = comidaSinDestace;
        this.materiaPrimaConDestace = materiaPrimaConDestace;
        this.materiaPrimaSinDestace = materiaPrimaSinDestace;
        this.precio = precio;
        this.tipoAnimal = tipoAnimal;
        this.edad = edad;
        this.fueDestazado = false;
        this.edadFinal = edad;
        
    }
    
    public String getNombreAnimal() {
        return nombreAnimal;
    }
    
    public void setNombreAnimal(String nombreAnimal) {
        this.nombreAnimal = nombreAnimal;
    }
    
    public void alimentarAnimal(int cantidadComida) {
        this.vida += cantidadComida;
    }
    
    public void restarVida() {
        this.vida--;
    }
    
    public void restarEdad() {
        this.edad--;
    }
    
    public boolean estaMuerto() {
        return this.vida <= 0;
    }
    
    public boolean murioDeViejo() {
        return edad <= 0;
    }
    
    public void fueDestazado(boolean fueDestazado) {
        this.fueDestazado = fueDestazado;
    }
    
    public int getVida() {
        return vida;
    }
    
    public double getEspacioOcupado() {
        return espacioOcupado;
    }
    
    public int getComidaDestace() {
        return comidaDestace;
    }
    
    public int getComidaSinDestace() {
        return comidaSinDestace;
    }
    
    public int getMateriaPrimaConDestace() {
        return materiaPrimaConDestace;
    }
    
    public int getMateriaPrimaSinDestace() {
        return materiaPrimaSinDestace;
    }
    
    public int getPrecio() {
        return precio;
    }
    
    public TipoAnimal getTipoAnimal() {
        return tipoAnimal;
    }
    
    public int getEdad() {
        return edad;
    }
    
    public void setVida(int vida) {
        this.vida = vida;
    }
    
    public void setEspacioOcupado(double espacioOcupado) {
        this.espacioOcupado = espacioOcupado;
    }
    
    public void setComidaDestace(int comidaDestace) {
        this.comidaDestace = comidaDestace;
    }
    
    public void setComidaSinDestace(int comidaSinDestace) {
        this.comidaSinDestace = comidaSinDestace;
    }
    
    public void setMateriaPrima(int materiaPrima) {
        this.materiaPrimaConDestace = materiaPrima;
    }
    
    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
    public boolean fueDestazado() {
        return fueDestazado;
    }
    
    public void setTipoAnimal(TipoAnimal tipoAnimal) {
        this.tipoAnimal = tipoAnimal;
    }
    
    public void setEdad(int edad) {
        this.edad = edad;
    }
    
    public String mostrarDatos() {
        
        return nombreAnimal + " precio: " + precio + "  Espacio a ocupar: " + espacioOcupado;
    }
    
    @Override
    public String toString() {
        return nombreAnimal;
    }
    
    public String verDatosDestaceAnimal() {
        
        return "Alimento con destace: " + comidaDestace + " Materia con destace: " + materiaPrimaConDestace;
    }
    
    public String verDatosSinDestace() {
        
        return "Alimento sin destace: " + comidaSinDestace + " Materia sin destace: " + materiaPrimaSinDestace;
    }
    
    public void iniciarVidaAnimal(CasillaGUI casillaGUI) {
        Thread vidaAnimal = new Thread() {
            
            @Override
            public void run() {
                try {
                    iniciarVidaAnimal();
                } catch (InterruptedException ex) {
                    
                }
            }
            
            void iniciarVidaAnimal() throws InterruptedException {
                
                while (!estaMuerto() && !murioDeViejo() && fueDestazado == false && GranjaGUI.cerrarTodo == false) {
                    Thread.sleep(1000);
                    restarVida();
                    restarEdad();
                    System.out.println("Vida animal  " + vida);
                    if (edad > (int) (edadFinal / 3)) {
                        comidaDestace += 2;
                        comidaSinDestace += 2;
                    } else if (edad > (int) (edadFinal / 3) && edad < (int) (2 * edadFinal / 3)) {
                        comidaDestace += 15;
                        comidaSinDestace += 15;
                    } else {
                        comidaDestace += 5;
                        comidaSinDestace += 5;
                    }
                    
                    if (murioDeViejo()) {
                        casillaGUI.setBackgroundImage(ImagesEnum.ANIMAL_MUERTO);
                        JOptionPane.showMessageDialog(null, "Se murió");
                    }
                    
                }
            }
        };
        vidaAnimal.start();
        
    }
    
}
