package com.ipc1.proyecto1.logica;

import java.util.Arrays;

public class SueloGrama extends Suelo {

    private Planta semilla;
    private int posicionSemilla;
    private int tiempoCosecha;
    private int tiempoProdrirse;
    private int tiempoVida;
    
    
    
    
    public SueloGrama(int fila, int columna, int numero) {
        super(fila, columna, numero,TipoSuelo.GRAMA);
        iniciarSuelo();
    }

    public void iniciarSuelo() {
        this.semilla = null;
    }

    public void Sembrar(Planta semilla) {
        this.tiempoCosecha=semilla.getTiempoCosecha();
        this.tiempoProdrirse=semilla.getTiempoPodrirse();
        this.tiempoVida=0;
        this.semilla=semilla;
    }

    @Override
    public void producirComida() {

    }

    @Override
    public int limpiarTerreno() {
        int precioLimpieza = 30;
        return precioLimpieza;
    }

    public int getTiempoCosecha() {
        return tiempoCosecha;
    }

    public int getTiempoProdrirse() {
        return tiempoProdrirse;
    }

    public int getTiempoVida() {
        return tiempoVida;
    }
    
    public void envejecerSuelo(){
        this.tiempoVida++;
    }
    
    public boolean esTiempoParaCosechar(){
        return tiempoVida>=tiempoCosecha;
    }
    
    public boolean esTiempoPodrirse(){
        return tiempoVida>=tiempoCosecha+tiempoProdrirse;
    }

    public Planta getSemilla() {
        return semilla;
    }

}
