package com.ipc1.proyecto1.logica;

public class Planta {

    private TipoPlanta tipoProduccion;
    private String nombrePlanta;
    private int cantidadSemillas;
    private int precioSemilla;
    private int tiempoCosecha;
    private int tiempoPodrirse;
    private int alimentoProducido;

    public Planta(TipoPlanta tipoProduccion, String nombrePlanta, int cantidadSemillas, int precioSemilla, int tiempoCosecha, int tiempoPodrirse, int alimentoProducido) {

        this.tipoProduccion = tipoProduccion;
        this.nombrePlanta = nombrePlanta;
        this.cantidadSemillas = cantidadSemillas;
        this.precioSemilla = precioSemilla;
        this.tiempoCosecha = tiempoCosecha;
        this.tiempoPodrirse = tiempoPodrirse;
        this.alimentoProducido = alimentoProducido;
    }

    public TipoPlanta getTipoProduccion() {
        return tipoProduccion;
    }

    public void setTipoProduccion(TipoPlanta tipoProduccion) {
        this.tipoProduccion = tipoProduccion;
    }

    public String getNombrePlanta() {
        return nombrePlanta;
    }

    public void setNombrePlanta(String nombrePlanta) {
        this.nombrePlanta = nombrePlanta;
    }

    public void setCantidadSemillas(int cantidadSemillas) {
        this.cantidadSemillas = cantidadSemillas;
    }

    public void setPrecioSemilla(int precioSemilla) {
        this.precioSemilla = precioSemilla;
    }

    public void setTiempoCosecha(int tiempoCosecha) {
        this.tiempoCosecha = tiempoCosecha;
    }

    public void setTiempoPodrirse(int tiempoPodrirse) {
        this.tiempoPodrirse = tiempoPodrirse;
    }

    public void setAlimentoProducido(int alimentoProducido) {
        this.alimentoProducido = alimentoProducido;
    }

    public int getCantidadSemillas() {
        return cantidadSemillas;
    }

    public int getPrecioSemilla() {
        return precioSemilla;
    }

    public int getTiempoCosecha() {
        return tiempoCosecha;
    }

    public int getTiempoPodrirse() {
        return tiempoPodrirse;
    }

    public int getAlimentoProducido() {
        return alimentoProducido;
    }

    public void aplicarFertilizante(int fertilizante) {
        this.alimentoProducido *= fertilizante;
    }

    public String mostrarDatos() {
        return nombrePlanta + " semillas necesarias: " + cantidadSemillas + " Precio de semilla: " + precioSemilla + " Tiempo de cosecha: " + tiempoCosecha;
    }

}
