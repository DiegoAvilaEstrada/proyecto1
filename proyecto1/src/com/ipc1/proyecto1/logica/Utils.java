package com.ipc1.proyecto1.logica;

import java.awt.Component;
import javax.swing.JPanel;

public class Utils {

    public static int generarAleatorio() {
        return (int) ((Math.random() * 25) + 1);
    }

    public static int[] generarListaNumerosAleatorios(int tamanoLista) {
        //GENERAMOS PNUMEROS ALEATORIOS SIN RPETIRSE
        int[] numerosRandom = iniciarArreglo(tamanoLista);

        for (int i = 0; i < tamanoLista; i++) {
            if (i == 0) {
                numerosRandom[i] = generarAleatorio();
                continue;
            }
            boolean yaExiste = false;
            int numeroGenerado;
            do {
                numeroGenerado = generarAleatorio();
                yaExiste = verificarSiUnNumeroExisteEnArray(numeroGenerado, numerosRandom);

            } while (yaExiste);
            numerosRandom[i] = numeroGenerado;
        }
        return numerosRandom;

    }

    private static boolean verificarSiUnNumeroExisteEnArray(int numero, int[] numeros) {
        for (int numeroTemporal : numeros) {
            if (numeroTemporal == numero) {
                return true;
            }
        }
        return false;
    }

    private static int[] iniciarArreglo(int tamano) {
        int numeros[] = new int[tamano];
        for (int i = 0; i < tamano; i++) {
            numeros[i] = -1;
        }
        return numeros;
    }

    public static void setPanelEnabled(JPanel panel, Boolean isEnabled) {
        panel.setEnabled(isEnabled);

        Component[] components = panel.getComponents();

        for (Component component : components) {
            if (component instanceof JPanel) {
                setPanelEnabled((JPanel) component, isEnabled);
            }
            component.setEnabled(isEnabled);
        }
    }

    public static String converirMilisegundosATexto(long tiempoTranscurridoPartida) {
        long segundosTotales = tiempoTranscurridoPartida / 1000l;
        long horas = segundosTotales / 3600;
        long minutos = (segundosTotales % 3600) / 60;
        long segundos = segundosTotales - horas * 3600 - minutos * 60;
        return horas + " : " + minutos + " : " + segundos;
    }
   
}
