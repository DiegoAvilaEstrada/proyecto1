package com.ipc1.proyecto1.logica;

public enum TipoSuelo {
    GRAMA(ImagesEnum.GRAMA),
    DESIERTO(ImagesEnum.DESIERTO),
    AGUA(ImagesEnum.AGUA);
    private final ImagesEnum imagen;

    TipoSuelo(ImagesEnum imagen) {
        this.imagen = imagen;
    }

    public ImagesEnum imagen() {
        return imagen;
    }

}
