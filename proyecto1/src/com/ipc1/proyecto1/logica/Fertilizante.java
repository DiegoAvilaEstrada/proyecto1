package com.ipc1.proyecto1.logica;

public enum Fertilizante {

    ABONO("Abono Sencillo", 2, 5),
    ABONO_MEDIO("Abono Medio", 3, 10),
    ABONO_PLUS("Abono Plus", 5, 15);

    private String nombreFertrilizante;
    private final int fertilizante;
    private final int precioFertrilizante;

    private Fertilizante(String nombreFertilizante, int fertilizante, int precioFertilizante) {
        this.nombreFertrilizante = nombreFertilizante;
        this.fertilizante = fertilizante;
        this.precioFertrilizante = precioFertilizante;
    }

    public String getNombreFertrilizante() {
        return nombreFertrilizante;
    }

    public int getFertilizante() {
        return fertilizante;
    }

    public int getPrecioFertilizante() {
        return precioFertrilizante;
    }

}
