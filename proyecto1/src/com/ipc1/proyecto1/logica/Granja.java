package com.ipc1.proyecto1.logica;

public class Granja {

    public final static int NUMERO_INICIAL_COLUMNAS = 5;
    public final static int NUMERO_INICIAL_FILAS = 5;
    public final static int NUMERO_INICIAL_CASILLAS = NUMERO_INICIAL_COLUMNAS * NUMERO_INICIAL_FILAS;

    public final static int NUMERO_MAXIMO_COLUMNAS = 7;
    public final static int NUMERO_MAXIMO_FILAS = 7;

    private Suelo[][] suelos;
    private Parcela[] parcelas;
    private Bodega bodega;

    public Granja() {
        this.parcelas = new Parcela[0];
        this.bodega = new Bodega();
        this.generarGranja();
    }

    private void generarGranja() {
        suelos = new Suelo[NUMERO_MAXIMO_COLUMNAS][NUMERO_MAXIMO_FILAS];
        int contador = 0;
        int numeroCasilla = 1;
        int[] numerosAleatorios = Utils.generarListaNumerosAleatorios(NUMERO_INICIAL_CASILLAS);

        for (int fila = 0; fila < NUMERO_MAXIMO_COLUMNAS; fila++) {
            for (int columna = 0; columna < NUMERO_MAXIMO_FILAS; columna++) {
                if (fila < NUMERO_INICIAL_FILAS && columna < NUMERO_INICIAL_COLUMNAS) {

                    if (numerosAleatorios[contador] <= 10) {
                        suelos[fila][columna] = new SueloGrama(fila, columna, numeroCasilla);
                    } else if (numerosAleatorios[contador] > 10 && numerosAleatorios[contador] <= 18) {
                        suelos[fila][columna] = new SueloAgua(fila, columna, numeroCasilla);
                    } else if (numerosAleatorios[contador] > 18 && numerosAleatorios[contador] <= 25) {
                        suelos[fila][columna] = new SueloDesierto(fila, columna, numeroCasilla);
                    }
                    contador++;
                }
                numeroCasilla++;
            }
        }
    }

    public Suelo generarCasillaAleatoria(int fila, int columna, int numero) {

        int numeroAleatorio = (int) ((Math.random() * 5) + 1);

        switch (numeroAleatorio) {
            case 1:
                suelos[fila][columna] = new SueloGrama(fila, columna, numero);
                break;
            case 2:
                suelos[fila][columna] = new SueloGrama(fila, columna, numero);
                break;
            case 3:
                suelos[fila][columna] = new SueloAgua(fila, columna, numero);
                break;
            case 4:
                suelos[fila][columna] = new SueloAgua(fila, columna, numero);
                break;
            case 5:
                suelos[fila][columna] = new SueloDesierto(fila, columna, numero);
                break;
        }
        return suelos[fila][columna];

    }

    public Suelo obtenerSuelo(int fila, int columna) {
        return suelos[columna][fila];
    }

    public void agrandarArregloYAñadir(Parcela parcela) {
        Parcela[] parcelasTemporal = new Parcela[this.parcelas.length + 1];
        for (int i = 0; i < this.parcelas.length; i++) {
            parcelasTemporal[i] = this.parcelas[i];
        }
        this.parcelas = parcelasTemporal;
        this.parcelas[this.parcelas.length - 1] = parcela;
    }

    public boolean existeEnOtraParcela(SueloGrama sueloGrama) {
        for (Parcela parcela : parcelas) {
            for (SueloGrama sueloGramaTemp : parcela.getSuelosGrama()) {
                if (sueloGrama.getNumero() == sueloGramaTemp.getNumero()) {
                    return true;
                }
            }
        }
        return false;
    }

    public Parcela[] getParcelas() {
        return parcelas;
    }

    public Bodega entrarBodega() {
        return bodega;
    }
    
    

}
