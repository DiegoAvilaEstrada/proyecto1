
package com.ipc1.proyecto1.logica;


public class Semilla {
    
    private final String planta;
    private final  int precio;

    public Semilla(String planta, int precio) {
        this.planta = planta;
        this.precio = precio;
    }

    
    public String getPlanta() {
        return planta;
    }

    public double getPrecio() {
        return precio;
    }
    
    
    
    
}
