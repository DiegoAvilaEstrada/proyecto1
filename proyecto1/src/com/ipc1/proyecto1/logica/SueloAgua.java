package com.ipc1.proyecto1.logica;

public class SueloAgua extends Suelo {

    private Barco barco;
    private int tiempoDePesca;
    private int tiempoGenerarPez;
    private boolean primeraVezGenerado = true;
    public final static int ALIMENTO_PEZ = 25;


    public SueloAgua(int fila, int columna, int numero) {
        super(fila, columna, numero, TipoSuelo.AGUA);
        iniciarNuevoBarco();
    }

    @Override
    public void producirComida() {

    }

    public void iniciarNuevoBarco() {
        this.barco = null;

    }

    public void iniciarPesca(int tiempoPesca) {
        this.tiempoDePesca = tiempoPesca;
    }

    public void barcoPescando() {
        this.tiempoDePesca--;
    }

    public boolean esTiempoDeRecogerPesca() {

        return tiempoDePesca <= 0;
    }

    public void establecerPez() {

        this.tiempoGenerarPez = 15;
    }

    public void generandoPez() {
        this.tiempoGenerarPez--;
    }

    public boolean esTiempoDePescar() {

        return tiempoGenerarPez <= 0;
    }

    public int getTiempoDePesca() {
        return tiempoDePesca;
    }

    public int getTiempoGenerarPez() {
        return tiempoGenerarPez;
    }
    
    

    public void crearBarco(Barco barco) {
        this.barco = barco;
        tiempoDePesca = 10;
    }

    public Barco getBarco() {
        return barco;
    }

    @Override
    public int limpiarTerreno() {
        return 0;
    }

    public boolean isPrimeraVezGenerado() {
        return primeraVezGenerado;
    }

    public void setPrimeraVezGenerado(boolean primeraVezGenerado) {
        this.primeraVezGenerado = primeraVezGenerado;
    }

}
