package com.ipc1.proyecto1.logica;

public class Bodega {

    private int alimentoRecogidoPlanta;
    private int alimentoRecogidoAnimal;
    private int materiaPrimaRecogida;

    public void Bodega() {
        alimentoRecogidoPlanta = 0;
        alimentoRecogidoAnimal = 0;
        materiaPrimaRecogida = 0;
    }

    public int getAlimentoRecogidoPlanta() {
        return alimentoRecogidoPlanta;
    }

    public int getMateriaPrimaRecogida() {
        return materiaPrimaRecogida;
    }

    public int getAlimentoRecogidoAnimal() {
        return alimentoRecogidoAnimal;
    }

    public void recogerAlimentoPlanta(int alimentoRecogido) {
        this.alimentoRecogidoPlanta += alimentoRecogido;
    }

    public void recogerAlimentoAnimal(int alimentoRecogido) {
        this.alimentoRecogidoAnimal += alimentoRecogido;
    }

    public void recogerMateriaPrima(int materiaPrimaRecogida) {
        this.materiaPrimaRecogida += materiaPrimaRecogida;
    }

    public void retirarAlimentoPlanta(int alimentoRetirado) {
        this.alimentoRecogidoPlanta -= alimentoRetirado;
    }

    public void retirarAlimentoAnimal(int alimentoRetirado) {
        this.alimentoRecogidoAnimal -= alimentoRetirado;
    }

    public void venderMateriaPrima(int materiaPrimaVendida) {
        this.materiaPrimaRecogida -= materiaPrimaVendida;
    }

}
