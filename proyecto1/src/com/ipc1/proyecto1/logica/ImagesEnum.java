package com.ipc1.proyecto1.logica;

public enum ImagesEnum {

    CAMPO_SEMBRADO("campoSembrado.jpg"),
    MAIZ("cosechaMaiz.jpg"),
    BARCO("barco.png"),
    PARCELA("parcela.jpg"),
    PEZ("peces.jpg"),
    GRAMA("Grama.jpg"),
    DESIERTO("Desierto.jpg"),
    AGUA("Agua.jpg"),
    GALLINA("Gallina.jpg"),
    CASILLA_DISPONIBLE("CasillaDisponible.jpg"),
    MANZANO("cosechaManzana.jpg"),
    FRUTA("cosechaFruta.jpg"),
    GRANO("cosechaGrano.jpg"),
    COSECHA_PODRIDA("cosechaPodrida.png"),
    VACA("Vaca.png"),
    ANIMAL_MUERTO("animalMuerto.png");

    private final String imagen;

    ImagesEnum(String imagen) {
        this.imagen = imagen;
    }

    public String obtenerImagen() {
        return imagen;
    }
}
