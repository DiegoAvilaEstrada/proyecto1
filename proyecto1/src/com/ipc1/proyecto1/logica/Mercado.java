package com.ipc1.proyecto1.logica;

public class Mercado {

    private final Animal crias[];
    private final Planta semillas[];
    private int posicionCria;
    private int posicionSemilla;

    public Mercado(Animal[] animales, Planta[] plantas) {
        this.crias = animales;
        this.semillas = plantas;
        this.posicionCria = 0;
        this.posicionSemilla = 0;

    }

    public void venderAnimal(Animal animal, Granjero granjero) {

        if (granjero.getOro() > animal.getPrecio()) {

        }
    }

    public void venderSemillas(int cantidadSemillas, Granjero granjero) {
        int precio = 0;
        for (int x = 0; x < cantidadSemillas; x++) {
            precio += semillas[x].getPrecioSemilla();
        }

        if (granjero.getOro() >= precio * cantidadSemillas) {
            //añadimos la planta comprada a la celda seleccionada
        }

    }

    public void agregarCrias(Animal cria) {
        crias[posicionCria] = cria;
        posicionCria++;
    }

    public void agregarSemillas(Planta planta) {
        this.semillas[posicionSemilla] = planta;
        posicionSemilla++;
    }

    public Animal[] getCrias() {
        return crias;
    }

    public Planta[] getSemillas() {
        return semillas;
    }

    public int getPosicionSemilla() {
        return posicionSemilla;
    }

}
