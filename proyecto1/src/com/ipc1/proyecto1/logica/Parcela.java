package com.ipc1.proyecto1.logica;

import java.util.Arrays;

public class Parcela {

    //TODO: agregar animales, validar que sean del mismo tipo
    private SueloGrama[] suelosGrama;
    private Animal[] animales;

    private int numeroParcela;
    private double espacioParcela;

    public Parcela(int numeroParcela) {
        this.suelosGrama = new SueloGrama[0];
        this.animales = new Animal[0];
        this.numeroParcela = numeroParcela;
    }

    public void setEspacioParcela(double espacioParcela) {
        this.espacioParcela = espacioParcela;
    }

    public double getEspacioParcela() {
        return espacioParcela;
    }

    public void liberarEspacioParcela(double espacioDesocupado) {
        this.espacioParcela+=espacioDesocupado;
    }

    public boolean agregarSueloGrama(SueloGrama sueloGrama) {
        if ((this.suelosGrama.length == 0) || (!existeSueloGrama(sueloGrama) && esColindanteConExistentes(sueloGrama))) {
            return agrandarArregloYAñadir(sueloGrama);
        }
        return false;

    }

    private boolean existeSueloGrama(SueloGrama sueloGrama) {
        if (this.suelosGrama.length == 0) {
            return false;
        }
        for (int i = 0; i < this.suelosGrama.length; i++) {
            if (sueloGrama.getNumero() == this.suelosGrama[i].getNumero()) {
                return true;
            }
        }
        return false;
    }

    private boolean esColindanteConExistentes(SueloGrama sueloGrama) {
        if (this.suelosGrama.length == 0) {
            return false;
        }

        for (int i = 0; i < this.suelosGrama.length; i++) {
            if (sueloGrama.getNumero() == (this.suelosGrama[i].getNumero() + Granja.NUMERO_MAXIMO_FILAS)
                    || sueloGrama.getNumero() == (this.suelosGrama[i].getNumero() - Granja.NUMERO_MAXIMO_FILAS)) {
                return true;
            } else if (sueloGrama.getNumero() % Granja.NUMERO_MAXIMO_FILAS == 0 && sueloGrama.getNumero() == this.suelosGrama[i].getNumero() + 1) {
                return true;
            } else if (sueloGrama.getNumero() % Granja.NUMERO_MAXIMO_FILAS == 1 && sueloGrama.getNumero() == this.suelosGrama[i].getNumero() - 1) {
                return true;
            } else if (sueloGrama.getNumero() == this.suelosGrama[i].getNumero() - 1 || sueloGrama.getNumero() == this.suelosGrama[i].getNumero() + 1) {
                return true;
            }

        }
        return false;
    }

    private boolean agrandarArregloYAñadir(SueloGrama sueloGrama) {
        SueloGrama[] suelosGramaTemporal = new SueloGrama[this.suelosGrama.length + 1];
        for (int i = 0; i < this.suelosGrama.length; i++) {
            suelosGramaTemporal[i] = this.suelosGrama[i];
        }
        this.suelosGrama = suelosGramaTemporal;
        this.suelosGrama[this.suelosGrama.length - 1] = sueloGrama;
        return true;
    }

    public void agregarAnimal(Animal animal) {
        animales = Arrays.copyOf(animales, animales.length + 1);
        animales[animales.length - 1] = animal;
    }

    //animales es el arreglo original de la parcela, indice es la posicion del elemento que queremos eliminar
    public Animal[] eliminarAnimal(int indice) {
        Animal[] nuevosAnimales = new Animal[animales.length - 1];
        if (indice > 0) {
            System.arraycopy(animales, 0, nuevosAnimales, 0, indice);
        }
        if (nuevosAnimales.length > indice) {

            System.arraycopy(animales, indice + 1, nuevosAnimales, indice, nuevosAnimales.length - indice);
        }
        return nuevosAnimales;
    }

    public SueloGrama[] getSuelosGrama() {
        return suelosGrama;
    }

    public int getNumeroParcela() {
        return numeroParcela;
    }

    public void aumentarNumeroParcela() {
        numeroParcela++;
    }

    public Animal[] getAnimales() {
        return animales;
    }

    public void setAnimales(Animal animales[]) {
        this.animales = animales;
    }

}
