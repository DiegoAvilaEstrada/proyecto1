package com.ipc1.proyecto1.logica;

public enum AlimentoAnimalHerbiboro {

    PASTO(15, 5),
    HIERBA(25, 10),
    VEGETALES(35, 15);

    private final int alimentoHerbiboro;
    private final int precioAlimento;

    private AlimentoAnimalHerbiboro(int alimentoHerbiboro, int precioAlimento) {
        this.alimentoHerbiboro = alimentoHerbiboro;
        this.precioAlimento = precioAlimento;
    }

    public int getAlimentoHerbiboro() {
        return alimentoHerbiboro;
    }

    public int getPrecioAlimento() {
        return precioAlimento;
    }

}
