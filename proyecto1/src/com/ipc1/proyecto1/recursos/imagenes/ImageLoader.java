/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ipc1.proyecto1.recursos.imagenes;

import java.net.URL;

/**
 *
 * @author 
 */
public class ImageLoader {

    public static URL getImagePath(String nombreImagen) {
        return ImageLoader.class.getClassLoader().getResource("com/ipc1/proyecto1/recursos/imagenes/"+nombreImagen);
    }

}
