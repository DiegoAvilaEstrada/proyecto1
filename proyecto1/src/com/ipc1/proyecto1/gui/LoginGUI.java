package com.ipc1.proyecto1.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import com.ipc1.proyecto1.logica.Granjero;

public final class LoginGUI extends JFrame {

    private final JPanel panel = new JPanel();
    private final JTextField nombre = new JTextField();
    private final JTextField nickName = new JTextField();
    private GranjaGUI ventanaGranja;

    private Granjero granjero;

    public LoginGUI() {

        this.setSize(400, 400);
        this.setLocationRelativeTo(null);
        this.setResizable(false);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setTitle("Log_In");
        iniciarComponentes();
        
        iniciarValores();
                this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);


    }

    public void iniciarComponentes() {

        crearPanel();
        crearEtiqueta();
        crearText();
        crearBoton();

    }

    public void crearPanel() {

        panel.setLayout(null);
        panel.setBackground(Color.DARK_GRAY);
        this.getContentPane().add(panel);
    }

    public void crearEtiqueta() {

        JLabel miGranja = new JLabel("Granja ´El Pollito´", SwingConstants.LEFT);
        miGranja.setBounds(100, 50, 250, 35);
        miGranja.setForeground(Color.lightGray);
        miGranja.setFont(new Font("arial", 1, 25));
        panel.add(miGranja);

        JLabel etiquetaNombre = new JLabel("Nombre:");
        etiquetaNombre.setBounds(100, 100, 50, 50);
        etiquetaNombre.setForeground(Color.WHITE);
        panel.add(etiquetaNombre);

        JLabel etiquetaNick = new JLabel("NickName:");
        etiquetaNick.setBounds(100, 150, 100, 50);
        etiquetaNick.setForeground(Color.WHITE);
        panel.add(etiquetaNick);

    }

    public void crearText() {

        nombre.setBounds(160, 110, 100, 20);
        panel.add(nombre);

        nickName.setBounds(170, 163, 100, 20);
        panel.add(nickName);

    }

    public void crearBoton() {

        JButton ingresarUsuario = new JButton("Ingresar Usuario");
        ingresarUsuario.setBounds(115, 220, 150, 30);
        ingresarUsuario.setBackground(Color.DARK_GRAY);
        ingresarUsuario.setForeground(Color.WHITE);

        ActionListener ingresar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent ae) {
                if (nombre.getText().equals("") || nickName.getText().equals("")) {
                    JOptionPane.showMessageDialog(null, "No has rellenado las casillas por completo!");
                } else {
                    granjero = new Granjero(nombre.getText(), nickName.getText(), 1000, 100);
                    ventanaGranja = new GranjaGUI(granjero);
                    LoginGUI.this.setVisible(false);
                }

            }
        };
        ingresarUsuario.addActionListener(ingresar);
        panel.add(ingresarUsuario);

    }

    public Granjero retornarGranjero() {
        return this.granjero;
    }

    
    private void iniciarValores(){
        nombre.setText("Diego Jose");
        nickName.setText("Diuf");
    }
}
