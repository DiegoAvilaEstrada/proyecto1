package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Granjero;
import com.ipc1.proyecto1.logica.Planta;
import java.awt.Color;
import javax.swing.JLabel;

public class ReportesGUI extends javax.swing.JFrame {

    GranjaGUI granjaGUI;

    public ReportesGUI(GranjaGUI granja) {
        initComponents();
        this.granjaGUI = granja;
        crearEtiquetasPlantas();
        crearEtiquetasAnimales();
        iniciarComponentes();
    }

    public void iniciarComponentes() {
        etiquetaNombreGranjero.setText(granjaGUI.getGranjero().getNombre());
        etiquetaOroAcumulado.setText("" + MercadoGUI.oroGenerado);
        etiquetaAlimentoConsumido.setText("" + Granjero.alimentoConsumido);
        etiquetaAlimentoGeneradoGranja.setText("" + GranjaGUI.alimentoGenerado);
        etiquetaCeldasCompradas.setText("" + CasillaDisponible.cantidadCasillasCompradas);

    }

    public void crearEtiquetasPlantas() {

        for (int i = 0; i < granjaGUI.getMercado().getSemillas().length; i++) {
            JLabel etiquetaPlanta = new JLabel("" + granjaGUI.getMercado().getSemillas()[i].getNombrePlanta());
            etiquetaPlanta.setBounds(40, 60 + 25 * i, 150, 20);
            etiquetaPlanta.setForeground(Color.WHITE);
            panelPlantas.add(etiquetaPlanta);
        }
        for (int i = 0; i < granjaGUI.getMercado().getSemillas().length; i++) {
            JLabel etiquetaCantidadPlanta = new JLabel("" + granjaGUI.getSemillasAcumuladas()[i]);
            etiquetaCantidadPlanta.setBounds(190, 60 + 25 * i, 50, 20);
            etiquetaCantidadPlanta.setForeground(Color.WHITE);
            panelPlantas.add(etiquetaCantidadPlanta);
        }

        for (int i = 0; i < granjaGUI.getMercado().getSemillas().length; i++) {
            JLabel etiquetaSemillaSembrada = new JLabel("" + granjaGUI.getSemillasSembradas()[i]);
            etiquetaSemillaSembrada.setBounds(250, 60 + 25 * i, 50, 20);
            etiquetaSemillaSembrada.setForeground(Color.WHITE);
            panelPlantas.add(etiquetaSemillaSembrada);
        }
    }

    public void crearEtiquetasAnimales() {
        for (int i = 0; i < granjaGUI.getMercado().getCrias().length; i++) {
            JLabel etiquetaAnimal = new JLabel("" + granjaGUI.getMercado().getCrias()[i].getNombreAnimal());
            etiquetaAnimal.setBounds(40, 60 + 25 * i, 150, 20);
            etiquetaAnimal.setForeground(Color.WHITE);
            panelAnimales.add(etiquetaAnimal);
        }

        for (int i = 0; i < granjaGUI.getMercado().getCrias().length; i++) {
            JLabel etiquetaAnimalComprado = new JLabel("" + granjaGUI.getAnimalesComprados()[i]);
            etiquetaAnimalComprado.setBounds(190, 60 + 25 * i, 50, 20);
            etiquetaAnimalComprado.setForeground(Color.WHITE);
            panelAnimales.add(etiquetaAnimalComprado);
        }

        JLabel etiquetaAnimalDestazado = new JLabel("Total animales destazados: " + GranjaGUI.animalesDestazados);
        etiquetaAnimalDestazado.setBounds(240, 60 + 25, 50, 20);
        panelAnimales.add(etiquetaAnimalDestazado);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        etiquetaNombreGranjero = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        etiquetaOroAcumulado = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        etiquetaAlimentoGeneradoGranja = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        etiquetaDuracionPartida = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        etiquetaAlimentoConsumido = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        etiquetaCeldasCompradas = new javax.swing.JLabel();
        panelPlantas = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        panelAnimales = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("REPORTES");

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Reportes de Partida", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dubai", 0, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Nombre Granjero: ");

        etiquetaNombreGranjero.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaNombreGranjero.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Oro generado: ");

        etiquetaOroAcumulado.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaOroAcumulado.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("Alimento Generado: ");

        etiquetaAlimentoGeneradoGranja.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoGeneradoGranja.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Duracion de la Partida: ");

        etiquetaDuracionPartida.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaDuracionPartida.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel11.setForeground(new java.awt.Color(255, 255, 255));
        jLabel11.setText("Alimento consumido por granjero: ");

        etiquetaAlimentoConsumido.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoConsumido.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Celdas compradas: ");

        etiquetaCeldasCompradas.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaCeldasCompradas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(etiquetaOroAcumulado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(etiquetaAlimentoConsumido, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 44, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(18, 18, 18)
                                .addComponent(etiquetaCeldasCompradas, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(etiquetaAlimentoGeneradoGranja, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(etiquetaDuracionPartida, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(38, 38, 38))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(26, 26, 26)
                        .addComponent(etiquetaNombreGranjero, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(etiquetaOroAcumulado, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(etiquetaAlimentoGeneradoGranja, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel8)
                                        .addComponent(jLabel2))
                                    .addComponent(etiquetaNombreGranjero, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(26, 26, 26)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel13)
                                    .addComponent(jLabel6)))
                            .addComponent(etiquetaCeldasCompradas, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel4)
                        .addComponent(jLabel11))
                    .addComponent(etiquetaDuracionPartida, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaAlimentoConsumido, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(55, 55, 55))
        );

        panelPlantas.setBackground(new java.awt.Color(51, 51, 51));
        panelPlantas.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder("Reportes de Plantas"), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dubai", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jLabel3.setText("COMRADAS");

        jLabel5.setText("CELDAS SEMBRADAS ");

        javax.swing.GroupLayout panelPlantasLayout = new javax.swing.GroupLayout(panelPlantas);
        panelPlantas.setLayout(panelPlantasLayout);
        panelPlantasLayout.setHorizontalGroup(
            panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPlantasLayout.createSequentialGroup()
                .addContainerGap(106, Short.MAX_VALUE)
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(59, 59, 59)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
        );
        panelPlantasLayout.setVerticalGroup(
            panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPlantasLayout.createSequentialGroup()
                .addGroup(panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        panelAnimales.setBackground(new java.awt.Color(51, 51, 51));
        panelAnimales.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Reportes Animales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dubai", 0, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        javax.swing.GroupLayout panelAnimalesLayout = new javax.swing.GroupLayout(panelAnimales);
        panelAnimales.setLayout(panelAnimalesLayout);
        panelAnimalesLayout.setHorizontalGroup(
            panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 511, Short.MAX_VALUE)
        );
        panelAnimalesLayout.setVerticalGroup(
            panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 164, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(467, 467, 467)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 133, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 67, Short.MAX_VALUE)
                .addComponent(panelPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(261, 261, 261))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelPlantas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelAnimales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel etiquetaAlimentoConsumido;
    private javax.swing.JLabel etiquetaAlimentoGeneradoGranja;
    private javax.swing.JLabel etiquetaCeldasCompradas;
    private javax.swing.JLabel etiquetaDuracionPartida;
    private javax.swing.JLabel etiquetaNombreGranjero;
    private javax.swing.JLabel etiquetaOroAcumulado;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel panelAnimales;
    private javax.swing.JPanel panelPlantas;
    // End of variables declaration//GEN-END:variables
}
