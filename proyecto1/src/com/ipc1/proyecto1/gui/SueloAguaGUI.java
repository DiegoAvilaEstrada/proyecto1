package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Barco;
import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.SueloAgua;
import javax.swing.JOptionPane;

public class SueloAguaGUI extends javax.swing.JFrame {

    private final CasillaGUI casillaGUI;
    private final GranjaGUI parent;


    public SueloAguaGUI(CasillaGUI casillaGUI, GranjaGUI ventanaGranja) {
        this.casillaGUI = casillaGUI;
        this.parent = ventanaGranja;
        initComponents();
    }

    public void hiloPescar() {
        Thread hiloPescar = new Thread() {

            @Override
            public void run() {

                try {
                    while (((SueloAgua) casillaGUI.getSuelo()).getTiempoDePesca() >= 0 && GranjaGUI.cerrarTodo == false) {
                        Thread.sleep(1000);
                        ((SueloAgua) casillaGUI.getSuelo()).barcoPescando();
                        System.out.println("Tiempo: " + ((SueloAgua) casillaGUI.getSuelo()).getTiempoDePesca());
                    }

                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                JOptionPane.showMessageDialog(null, "En la casilla " + casillaGUI.getNumeroCasilla() + " el barco ya pescó");

            }
        };
        hiloPescar.start();
    }

    public void hiloAñadirPez() {
        Thread hiloPez = new Thread() {

            @Override
            public void run() {

                try {
                    ((SueloAgua) casillaGUI.getSuelo()).establecerPez();
                    while (((SueloAgua) casillaGUI.getSuelo()).getTiempoGenerarPez() >= 0 && GranjaGUI.cerrarTodo == false) {
                        Thread.sleep(1000);
                        ((SueloAgua) casillaGUI.getSuelo()).generandoPez();
                    }

                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

                if (((SueloAgua) casillaGUI.getSuelo()).esTiempoDePescar()) {
                    casillaGUI.setBackgroundImage(ImagesEnum.PEZ);
                }
            }
        };
        hiloPez.start();
    }

    public void comprarBarco() {
        SueloAgua sueloAgua=(SueloAgua) casillaGUI.getSuelo();
        
        if (sueloAgua.getBarco() != null) {
            JOptionPane.showMessageDialog(null, "Ya hay un barco pescando");
            return;
        }
        
        if ( (sueloAgua.getTiempoGenerarPez() <= 0 || sueloAgua.isPrimeraVezGenerado())) {
            Barco barco = new Barco();
            if (parent.getGranjero().getOro() >= barco.getPRECIO()) {
                ((SueloAgua) casillaGUI.getSuelo()).crearBarco(barco);
                casillaGUI.setBackgroundImage(ImagesEnum.BARCO);
                parent.getGranjero().gastarOro(barco.getPRECIO());
                parent.getEtiquetaOroGranjero().setText("" + parent.getGranjero().getOro());
                JOptionPane.showMessageDialog(null, "Se ha creado un barco!");
                this.setVisible(false);
                ((SueloAgua) casillaGUI.getSuelo()).iniciarPesca(10);
                hiloPescar();
            } else {
                JOptionPane.showMessageDialog(null, "No tienes más monedas de oro");
            }
        } else {
            JOptionPane.showMessageDialog(null, "Ya hay un barco pescando");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        botonCrearBarco = new javax.swing.JButton();
        botonRecogerPesca = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("OPCIONES   AGUA");

        botonCrearBarco.setText("AÑADIR BARCO PESQUERO");
        botonCrearBarco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCrearBarcoActionPerformed(evt);
            }
        });

        botonRecogerPesca.setText("RECOGER PESCA");
        botonRecogerPesca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRecogerPescaActionPerformed(evt);
            }
        });

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Precio Barco: 150 monedas");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(168, 168, 168)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(123, 123, 123)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonCrearBarco, javax.swing.GroupLayout.DEFAULT_SIZE, 227, Short.MAX_VALUE)
                            .addComponent(botonRecogerPesca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(84, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(53, 53, 53)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonCrearBarco)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonRecogerPesca)
                .addContainerGap(77, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonCrearBarcoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCrearBarcoActionPerformed
        comprarBarco();
    }//GEN-LAST:event_botonCrearBarcoActionPerformed

    private void botonRecogerPescaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRecogerPescaActionPerformed
        if (((SueloAgua) casillaGUI.getSuelo()).esTiempoDeRecogerPesca() && ((SueloAgua) casillaGUI.getSuelo()).getBarco() != null) {
            parent.getGranja().entrarBodega().recogerAlimentoAnimal(SueloAgua.ALIMENTO_PEZ);
            parent.getBodegaGUI().getEtiquetaAlimentoProducidoAnimal().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
            parent.getBodegaGUI().getMercadoGUI().getEtiquetaAlimentoProducidoAnimales().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
            JOptionPane.showMessageDialog(null, "Pesca exitosa!");
            ((SueloAgua) casillaGUI.getSuelo()).iniciarNuevoBarco();
            casillaGUI.setBackgroundImage(ImagesEnum.AGUA);
            ((SueloAgua) casillaGUI.getSuelo()).setPrimeraVezGenerado(false);
            hiloAñadirPez();
            this.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(null, "No se puede recoger pesca");
        }
    }//GEN-LAST:event_botonRecogerPescaActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonCrearBarco;
    private javax.swing.JButton botonRecogerPesca;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
