package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Granjero;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class BodegaGUI extends javax.swing.JFrame {

    private MercadoGUI mercadoGUI;
    private GranjaGUI parent;

    public BodegaGUI(MercadoGUI mercadoGUI, GranjaGUI parent) {
        this.mercadoGUI = mercadoGUI;
        this.parent = parent;
        initComponents();
    }

    public MercadoGUI getMercadoGUI() {
        return mercadoGUI;
    }

    public JLabel getEtiquetaAlimentoProducidoPlanta() {
        return etiquetaAlimentoProducidoPlanta;
    }

    public JLabel getEtiquetaAlimentoProducidoAnimal() {
        return etiquetaAlimentoProducidoAnimal;
    }

    public JLabel getEtiquetaMateriaProducida() {
        return etiquetaMateriaProducida;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        etiquetaAlimentoProducidoPlanta = new javax.swing.JLabel();
        botonAlimentar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        etiquetaMateriaProducida = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        etiquetaAlimentoProducidoAnimal = new javax.swing.JLabel();
        comboAlimentarGranjero = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("B O D E G A");

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Alimento producido por plantas");

        etiquetaAlimentoProducidoPlanta.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoProducidoPlanta.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        botonAlimentar.setBackground(new java.awt.Color(51, 51, 51));
        botonAlimentar.setForeground(new java.awt.Color(255, 255, 255));
        botonAlimentar.setText("ALIMENTAR   AL   GRANJERO");
        botonAlimentar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAlimentarActionPerformed(evt);
            }
        });

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Materia Prima producida");

        etiquetaMateriaProducida.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaMateriaProducida.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Alimento producido por animales");

        etiquetaAlimentoProducidoAnimal.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoProducidoAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        comboAlimentarGranjero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Alimento Planta", "Alimento Animal" }));

        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Alimentar al granjero con");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 6, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 24, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonAlimentar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboAlimentarGranjero, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 169, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(etiquetaMateriaProducida, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(etiquetaAlimentoProducidoPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(0, 0, Short.MAX_VALUE)
                                        .addComponent(etiquetaAlimentoProducidoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)))))))
                .addGap(144, 144, 144))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(etiquetaAlimentoProducidoPlanta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(etiquetaAlimentoProducidoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(26, 26, 26)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(etiquetaMateriaProducida, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(33, 33, 33)
                        .addComponent(comboAlimentarGranjero, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonAlimentar)
                .addGap(26, 26, 26))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAlimentarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAlimentarActionPerformed
        switch (comboAlimentarGranjero.getSelectedIndex()) {
            case 0:
                if (parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta() > 0) {
                    parent.getGranjero().comer(1);
                    parent.getEtiquetaVidaGranjero().setText("" + parent.getGranjero().getVida());
                    parent.getGranja().entrarBodega().retirarAlimentoPlanta(1);
                    etiquetaAlimentoProducidoPlanta.setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta());
                    mercadoGUI.getEtiquetaAlimentoProducidoPlantas().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta());
                    Granjero.alimentoConsumido++;
                } else {
                    JOptionPane.showMessageDialog(null, "No hay más alimento de plantas");
                }
                break;
            case 1:
                if (parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal() > 0) {
                    parent.getGranjero().comer(1);
                    parent.getEtiquetaVidaGranjero().setText("" + parent.getGranjero().getVida());
                    parent.getGranja().entrarBodega().retirarAlimentoAnimal(1);
                    etiquetaAlimentoProducidoAnimal.setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                    mercadoGUI.getEtiquetaAlimentoProducidoAnimales().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                    Granjero.alimentoConsumido++;
                } else {
                    JOptionPane.showMessageDialog(null, "No hay más alimento de animales");
                }
                break;
            default:
                break;
        }
    }//GEN-LAST:event_botonAlimentarActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAlimentar;
    private javax.swing.JComboBox<String> comboAlimentarGranjero;
    private javax.swing.JLabel etiquetaAlimentoProducidoAnimal;
    private javax.swing.JLabel etiquetaAlimentoProducidoPlanta;
    private javax.swing.JLabel etiquetaMateriaProducida;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
