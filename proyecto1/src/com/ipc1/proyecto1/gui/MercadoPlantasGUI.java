package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Animal;
import com.ipc1.proyecto1.logica.Fertilizante;
import com.ipc1.proyecto1.logica.Planta;
import com.ipc1.proyecto1.logica.Utils;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MercadoPlantasGUI extends javax.swing.JFrame {

    private GranjaGUI ventanaGranja;
    private String nombreAnimales[];
    private JComboBox listaPlantas;
    private Planta semilla;

    public MercadoPlantasGUI(GranjaGUI ventanaGranja) {
        this.ventanaGranja = ventanaGranja;
        initComponents();
        listarPlantas();
        listarFertilizantes();
    }

    public void agregarListaPlantas(JComboBox listaPlantas) {
        this.listaPlantas = listaPlantas;
    }

    public void verDatosPlantas() {
        for (Planta semilla : ventanaGranja.getMercado().getSemillas()) {
            if (comboPlantas.getSelectedItem().toString().equals(semilla.getNombrePlanta())) {
                datosPlanta.setText(semilla.mostrarDatos());
            }
        }

    }

    private void listarPlantas() {
        for (Planta semilla : ventanaGranja.getMercado().getSemillas()) {
            comboPlantas.addItem(semilla.getNombrePlanta());
        }
    }

    private void listarFertilizantes() {
        comboFertilizantes.addItem(Fertilizante.ABONO.getNombreFertrilizante());
        comboFertilizantes.addItem(Fertilizante.ABONO_MEDIO.getNombreFertrilizante());
        comboFertilizantes.addItem(Fertilizante.ABONO_PLUS.getNombreFertrilizante());

    }

    public void comprarSemillas() {

        int indice = comboPlantas.getSelectedIndex();
        semilla = ventanaGranja.getMercado().getSemillas()[indice];
        if (ventanaGranja.getGranjero().getOro() >= semilla.getPrecioSemilla()) {
            ventanaGranja.getGranjero().adquirirSemilla(semilla);
            JOptionPane.showMessageDialog(null, "Se ha comprado una semilla de: " + ventanaGranja.getMercado().getSemillas()[indice].getNombrePlanta());
            ventanaGranja.getGranjero().gastarOro(semilla.getPrecioSemilla());
            ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
            ventanaGranja.getConfiguracionPlanta().getContadorSemillas()[indice]++;
            ventanaGranja.getSemillasAcumuladas()[indice]++;
            ventanaGranja.getSemillasCompradas()[indice].setText("" + ventanaGranja.getConfiguracionPlanta().getContadorSemillas()[indice]);
        } else {
            JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
        }
    }

    public void comprarFertilizante() {

        switch (comboFertilizantes.getSelectedIndex()) {
            case 0:
                semilla.aplicarFertilizante(Fertilizante.ABONO.getFertilizante());
                ventanaGranja.getGranjero().gastarOro(Fertilizante.ABONO.getPrecioFertilizante());
                JOptionPane.showMessageDialog(null, "Se ha aumentado la cantidad de produccion con el Fertilizante: " + Fertilizante.ABONO.getNombreFertrilizante());
                break;
            case 1:
                semilla.aplicarFertilizante(Fertilizante.ABONO_MEDIO.getFertilizante());
                ventanaGranja.getGranjero().gastarOro(Fertilizante.ABONO_MEDIO.getPrecioFertilizante());
                ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                JOptionPane.showMessageDialog(null, "Se ha aumentado la cantidad de produccion con el Fertilizante: " + Fertilizante.ABONO_MEDIO.getNombreFertrilizante());
                break;
            case 2:
                semilla.aplicarFertilizante(Fertilizante.ABONO_PLUS.getFertilizante());
                ventanaGranja.getGranjero().gastarOro(Fertilizante.ABONO_PLUS.getPrecioFertilizante());
                ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                JOptionPane.showMessageDialog(null, "Se ha aumentado la cantidad de produccion con el Fertilizante: " + Fertilizante.ABONO_PLUS.getNombreFertrilizante());
                break;
            default:
                break;
        }
    }

    public Planta getSemilla() {
        return semilla;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        comboPlantas = new javax.swing.JComboBox<>();
        botonComprarPlantas = new javax.swing.JButton();
        datosPlanta = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        comboFertilizantes = new javax.swing.JComboBox<>();
        botonComprarFertilizantes = new javax.swing.JButton();

        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 30)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Mercado de Plantas");

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Comprar Semillas"));
        jPanel2.setForeground(new java.awt.Color(51, 51, 51));

        comboPlantas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboPlantasActionPerformed(evt);
            }
        });

        botonComprarPlantas.setText("COMPRAR");
        botonComprarPlantas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarPlantasActionPerformed(evt);
            }
        });

        datosPlanta.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        datosPlanta.setForeground(new java.awt.Color(255, 255, 255));
        datosPlanta.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(102, 102, 102)));

        jLabel3.setFont(new java.awt.Font("Dubai", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Semillas");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(datosPlanta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(comboPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, 317, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botonComprarPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel3))
                .addGap(0, 6, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonComprarPlantas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(datosPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(51, 51, 51));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Comprar Fertilizantes"));
        jPanel3.setForeground(new java.awt.Color(51, 51, 51));

        jLabel2.setText("Fertilizantes");

        comboFertilizantes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Abono Simple (X2) Precio: 5 monedas", "Abono Medio  (X3) Precio: 10 monedas", "Abono Plus     (X5) Precio: 15 monedas", " " }));

        botonComprarFertilizantes.setText("COMPRAR ");
        botonComprarFertilizantes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarFertilizantesActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(comboFertilizantes, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(18, 18, 18)
                        .addComponent(botonComprarFertilizantes, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonComprarFertilizantes)
                    .addComponent(comboFertilizantes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComprarPlantasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarPlantasActionPerformed
        comprarSemillas();
    }//GEN-LAST:event_botonComprarPlantasActionPerformed

    private void comboPlantasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboPlantasActionPerformed
        verDatosPlantas();
    }//GEN-LAST:event_comboPlantasActionPerformed

    private void botonComprarFertilizantesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarFertilizantesActionPerformed
        comprarFertilizante();
        ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
    }//GEN-LAST:event_botonComprarFertilizantesActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonComprarFertilizantes;
    private javax.swing.JButton botonComprarPlantas;
    private javax.swing.JComboBox<String> comboFertilizantes;
    private javax.swing.JComboBox<String> comboPlantas;
    private javax.swing.JLabel datosPlanta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    // End of variables declaration//GEN-END:variables
}
