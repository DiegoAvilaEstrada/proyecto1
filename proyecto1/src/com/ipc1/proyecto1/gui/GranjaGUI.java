package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Granja;
import com.ipc1.proyecto1.logica.Granjero;
import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.Mercado;
import com.ipc1.proyecto1.logica.Planta;
import com.ipc1.proyecto1.logica.Suelo;
import com.ipc1.proyecto1.logica.SueloGrama;
import com.ipc1.proyecto1.logica.Utils;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;
import jdk.jshell.execution.Util;

public class GranjaGUI extends JFrame {

    private final Granja granja;
    private final JPanel panel;
    private final CasillaGUI casillas[][];
    private final Granjero granjero;
    private ConfiguracionAnimalesGUI configuracionAnimales;
    private ConfiguracionPlantaGUI configuracionPlanta;
    private MercadoPlantasGUI ventanaMercadoPlantas;
    private MercadoAnimalesGUI ventanaMercadoAnimales;
    private ReportesGUI reportesGUI;
    private Mercado mercado;
    private SueloGrama sueloGrama;
    private JLabel etiquetaGranjero;
    private JLabel etiquetaNickGranjero;
    private JLabel etiquetaOro;
    private JLabel etiquetaOroGranjero;
    private JLabel etiquetaSemillas;
    private JLabel etiquetaCantidadSemillas;
    private JButton botonCrearParcela;
    private JLabel semillasCompradas[];
    private JButton botonConsultarParcela;
    private JLabel etiquetaVida;
    private JLabel etiquetaVidaGranjero;
    private JLabel etiquetaTiempoPartida;
    private JButton botonBodega;
    private JButton botonMercado;
    private BodegaGUI bodegaGUI;
    private MercadoGUI mercadoGUI;
    private long tiempoInicioPartida;
    private Long tiempoFinPartida;

    public static boolean cerrarTodo;
    public static int alimentoGenerado;
    private int[] semillasAcumuladas;
    private int[] semillasSembradas;
    private int[] animalesComprados;
    public static int animalesDestazados;

    public GranjaGUI(Granjero granjero) {
        this.setVisible(false);
        this.setResizable(false);
        configurarAnimales();
        this.panel = new JPanel();
        this.casillas = new CasillaGUI[Granja.NUMERO_MAXIMO_FILAS][Granja.NUMERO_MAXIMO_COLUMNAS];
        this.granjero = granjero;
        this.setLayout(null);
        this.setSize(1200, 850);
        this.setLocationRelativeTo(null);
        granja = new Granja();
        mercadoGUI = new MercadoGUI(this);
        bodegaGUI = new BodegaGUI(this.mercadoGUI, this);
        this.setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new LiseterGranjaGUI());
        this.tiempoInicioPartida = System.currentTimeMillis();
        cerrarTodo = false;
        alimentoGenerado = 0;
        animalesDestazados = 0;
    }

    public void establecerSemillasAcumuladas(int cantidad) {
        semillasAcumuladas = new int[cantidad];
        for (int i = 0; i < semillasAcumuladas.length; i++) {
            semillasAcumuladas[i] = 0;
        }
    }

    public void establecerSemillasSembradas(int cantidad) {
        semillasSembradas = new int[cantidad];
        for (int i = 0; i < semillasSembradas.length; i++) {
            semillasSembradas[i] = 0;
        }
    }

    public void establecerAnimalesComprados(int cantidad) {
        animalesComprados = new int[cantidad];
        for (int i = 0; i < animalesComprados.length; i++) {
            animalesComprados[i] = 0;
        }
    }

    public int[] getSemillasAcumuladas() {
        return semillasAcumuladas;
    }

    public int[] getSemillasSembradas() {
        return semillasSembradas;
    }

    public int[] getAnimalesComprados() {
        return animalesComprados;
    }

    private void configurarAnimales() {
        this.configuracionAnimales = new ConfiguracionAnimalesGUI(this);
        this.configuracionAnimales.setVisible(true);

    }

    public void configurarPlantas() {
        this.configuracionPlanta = new ConfiguracionPlantaGUI(this);
        this.configuracionPlanta.setVisible(true);
    }

    public void accederVentanaMercadoPlantas(JComboBox listaPlantas) {
        this.ventanaMercadoPlantas = new MercadoPlantasGUI(this);
        ventanaMercadoPlantas.agregarListaPlantas(listaPlantas);
        ventanaMercadoPlantas.setVisible(true);

    }

    public void accederVentanaMercadoAnimales(CasillaGUI casillaGUI) {
        this.ventanaMercadoAnimales = new MercadoAnimalesGUI(casillaGUI, this);
        ventanaMercadoAnimales.setVisible(true);

    }

    public void accederReportes() {
        this.reportesGUI = new ReportesGUI(this);
        this.reportesGUI.setVisible(true);
    }

    public void iniciarComponentes() {
        this.setVisible(true);
        semillasCompradas = new JLabel[configuracionPlanta.getPlantas().length];
        crearPanel();
        crearGranjaInicial();
        crearMercado();
        hiloVidaGranjero();
        hiloTiempoPartida();
    }

    private void crearMercado() {
        mercado = new Mercado(configuracionAnimales.getAnimales(), configuracionPlanta.getPlantas());
    }

    public Mercado getMercado() {
        return this.mercado;
    }

    //acá tenemos nuestro panel inicial y nuestro panel de información
    public void crearPanel() {
        panel.setLayout(null);
        panel.setBounds(0, 0, this.getWidth(), this.getHeight());
        panel.setBackground(Color.DARK_GRAY);
        this.getContentPane().add(panel);

        etiquetaGranjero = new JLabel("Granjero: ");
        etiquetaGranjero.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, 20, 70, 20);
        etiquetaGranjero.setForeground(Color.WHITE);
        etiquetaNickGranjero = new JLabel(granjero.getNickName());
        etiquetaNickGranjero.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 100, 20, 60, 20);
        etiquetaNickGranjero.setForeground(Color.WHITE);
        etiquetaVida = new JLabel("Vida:");
        etiquetaVida.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 180, 20, 40, 20);
        etiquetaVida.setForeground(Color.WHITE);
        etiquetaVidaGranjero = new JLabel("" + granjero.getVida());
        etiquetaVidaGranjero.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 225, 20, 40, 20);
        etiquetaVidaGranjero.setForeground(Color.WHITE);
        etiquetaOro = new JLabel("Oro:");
        etiquetaOro.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, 45, 40, 20);
        etiquetaOro.setForeground(Color.WHITE);
        etiquetaOroGranjero = new JLabel("" + granjero.getOro());
        etiquetaOroGranjero.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 100, 45, 60, 20);
        etiquetaOroGranjero.setForeground(Color.WHITE);
        etiquetaTiempoPartida = new JLabel();
        etiquetaTiempoPartida.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 180, 45, 150, 20);
        etiquetaTiempoPartida.setForeground(Color.WHITE);
        etiquetaSemillas = new JLabel("Semillas en existencia:");
        etiquetaSemillas.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, 70, 150, 20);
        etiquetaSemillas.setForeground(Color.WHITE);
        panel.add(etiquetaGranjero);
        panel.add(etiquetaNickGranjero);
        panel.add(etiquetaOro);
        panel.add(etiquetaOroGranjero);
        panel.add(etiquetaSemillas);
        panel.add(etiquetaVida);
        panel.add(etiquetaVidaGranjero);
        panel.add(etiquetaTiempoPartida);

        for (int i = 0; i < configuracionPlanta.getPlantas().length; i++) {
            JLabel etiquetaSemillasCompradas = new JLabel(configuracionPlanta.getPlantas()[i].getNombrePlanta() + " =");
            etiquetaSemillasCompradas.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, 95 + 25 * i, 70, 20);
            etiquetaSemillasCompradas.setForeground(Color.WHITE);
            semillasCompradas[i] = new JLabel(("" + configuracionPlanta.getContadorSemillas()[i]));
            semillasCompradas[i].setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 90, 95 + 25 * i, 70, 20);
            semillasCompradas[i].setForeground(Color.WHITE);
            panel.add(etiquetaSemillasCompradas);
            panel.add(semillasCompradas[i]);
        }
        botonBodega = new JButton("BODEGA");
        botonBodega.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, this.getHeight() - 225, 150, 20);
        botonBodega.setBackground(Color.DARK_GRAY);
        botonBodega.setForeground(Color.WHITE);
        botonMercado = new JButton("MERCADO");
        botonMercado.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 170, this.getHeight() - 225, 150, 20);
        botonMercado.setBackground(Color.DARK_GRAY);
        botonMercado.setForeground(Color.WHITE);
        botonCrearParcela = new JButton("Crear Parcela");
        botonCrearParcela.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, this.getHeight() - 200, 150, 20);
        botonCrearParcela.setForeground(Color.WHITE);
        botonCrearParcela.setBackground(Color.DARK_GRAY);
        botonConsultarParcela = new JButton("Consultar Parcelas");
        botonConsultarParcela.setBounds(Granja.NUMERO_MAXIMO_COLUMNAS * CasillaGUI.TAMANO_CASILLA + 15, this.getHeight() - 150, 150, 20);
        botonConsultarParcela.setBackground(Color.DARK_GRAY);
        botonConsultarParcela.setForeground(Color.WHITE);
        panel.add(botonConsultarParcela);
        panel.add(botonCrearParcela);
        panel.add(botonBodega);
        panel.add(botonMercado);
        ingresarSueloGramaAParcela(botonCrearParcela);
        consultarParcela(botonConsultarParcela);
        ingresarBodega(botonBodega);
        ingresarMercado(botonMercado);

    }

    public void ingresarBodega(JButton boton) {
        ActionListener ingresar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                bodegaGUI.getEtiquetaAlimentoProducidoPlanta().setText("" + granja.entrarBodega().getAlimentoRecogidoPlanta());
                bodegaGUI.getEtiquetaMateriaProducida().setText("" + granja.entrarBodega().getMateriaPrimaRecogida());
                bodegaGUI.setVisible(true);
            }
        };
        boton.addActionListener(ingresar);
    }

    public void ingresarMercado(JButton boton) {
        ActionListener ingresar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                mercadoGUI.getEtiquetaAlimentoProducidoPlantas().setText("" + granja.entrarBodega().getAlimentoRecogidoPlanta());
                mercadoGUI.getEtiquetaAlimentoProducidoAnimales().setText("" + granja.entrarBodega().getAlimentoRecogidoAnimal());
                mercadoGUI.setVisible(true);
            }
        };
        boton.addActionListener(ingresar);

    }

    public void ingresarSueloGramaAParcela(JButton boton) {
        ActionListener ingresar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AdquirirParcelaGUI adquirirParcelaGUI = new AdquirirParcelaGUI(GranjaGUI.this);
                adquirirParcelaGUI.setVisible(true);
            }
        };
        boton.addActionListener(ingresar);
    }

    public void consultarParcela(JButton boton) {
        ActionListener consultar = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                accederReportes();
            }
        };
        boton.addActionListener(consultar);

    }

    public void crearGranjaInicial() { //Se genera una matriz con imagenes que representa el terreno de mi granja
        int numeroCasilla = 1;

        for (int fila = 0; fila < Granja.NUMERO_MAXIMO_FILAS; fila++) {
            for (int columna = 0; columna < Granja.NUMERO_MAXIMO_COLUMNAS; columna++) {

                if (fila < Granja.NUMERO_INICIAL_FILAS && columna < Granja.NUMERO_INICIAL_COLUMNAS) {
                    Suelo suelo = granja.obtenerSuelo(fila, columna);
                    casillas[fila][columna] = new CasillaGUI(suelo, this);
                } else {
                    casillas[fila][columna] = new CasillaGUI(fila, columna, numeroCasilla, this);
                    casillas[fila][columna].setBackgroundImage(ImagesEnum.CASILLA_DISPONIBLE);
                }
                numeroCasilla++;
                panel.add(casillas[fila][columna]);
            }
        }

    }

    public void hiloVidaGranjero() { //Este hilo nos determina la vida del Granjero
        Thread hiloVida = new Thread() {
            @Override
            public void run() {
                while (granjero.getVida() >= 0) {
                    try {
                        etiquetaVidaGranjero.setText("" + granjero.getVida());
                        Thread.sleep(3000);
                        granjero.restarVida(1);

                    } catch (InterruptedException ex) {
                        Logger.getLogger(GranjaGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                JOptionPane.showMessageDialog(null, "Se murio el granjero, la partida ha terminado!!!");
                cerrarTodo = true;
                GranjaGUI.this.setVisible(false);
                accederReportes();
                //TODO: mostrar ventana estadisicas
            }
        };

        hiloVida.start();

    }

    public void hiloTiempoPartida() { //Este hilo determina el tiempo de Duración de la Partida
        Thread hiloPartida = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        etiquetaTiempoPartida.setText(obtenerTiempoActualPartida());
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {

                    }

                }

            }
        };
        hiloPartida.start();
    }

    public void sembrarSemilla(Planta semilla) {
        sueloGrama.Sembrar(semilla);
    }

    public Granjero getGranjero() {
        return granjero;
    }

    public MercadoPlantasGUI getVentanaMercado() {
        return ventanaMercadoPlantas;
    }

    public JLabel getEtiquetaOroGranjero() {
        return etiquetaOroGranjero;
    }

    public Granja getGranja() {
        return granja;
    }

    public JPanel getPanel() {
        return panel;
    }

    public CasillaGUI getCasilla(int fila, int columna) {
        return casillas[fila][columna];
    }

    public JLabel getEtiquetaCantidadSemillas() {
        return etiquetaCantidadSemillas;
    }

    public JLabel[] getSemillasCompradas() {
        return semillasCompradas;
    }

    public ConfiguracionPlantaGUI getConfiguracionPlanta() {
        return configuracionPlanta;
    }

    public MercadoAnimalesGUI getVentanaMercadoAnimales() {
        return ventanaMercadoAnimales;
    }

    public JLabel getEtiquetaVidaGranjero() {
        return etiquetaVidaGranjero;
    }

    public BodegaGUI getBodegaGUI() {
        return bodegaGUI;
    }

    public MercadoGUI getMercadoGUI() {
        return mercadoGUI;
    }

    public ReportesGUI getReportesGUI() {
        return reportesGUI;
    }

    private String obtenerTiempoActualPartida() {
        if (tiempoFinPartida != null) {
            return Utils.converirMilisegundosATexto(tiempoFinPartida - tiempoInicioPartida);
        }
        return Utils.converirMilisegundosATexto(System.currentTimeMillis() - tiempoInicioPartida);
    }

    private class LiseterGranjaGUI extends WindowAdapter {

        @Override
        public void windowClosing(java.awt.event.WindowEvent windowEvent) {
            if (JOptionPane.showConfirmDialog(GranjaGUI.this,
                    "Quiere cerrar el juego?", "Close Window?",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION) {
                GranjaGUI.this.setVisible(false);
                cerrarTodo = true;
                accederReportes();
                tiempoFinPartida = System.currentTimeMillis();
            }

        }

    }

}
