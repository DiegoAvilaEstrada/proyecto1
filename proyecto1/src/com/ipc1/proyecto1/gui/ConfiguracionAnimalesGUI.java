package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Animal;
import com.ipc1.proyecto1.logica.TipoAnimal;
import com.ipc1.proyecto1.logica.Utils;
import javax.swing.JOptionPane;

public class ConfiguracionAnimalesGUI extends javax.swing.JFrame {

    private GranjaGUI ventanaGranja;
    private Animal animales[];
    private int cantidadAnimales;
    private int posicionAnimal;
    private double espacioCria;
    private int comidaDestace;
    private int comidaSinDestace;
    private int materiaPrimaConDestace;
    private int materiaPrimaSinDestace;
    private int precioCria;
    private TipoAnimal tipoCria;
    private int edad;
    private boolean error;

    public ConfiguracionAnimalesGUI(GranjaGUI ventanaGranja) {
        this.ventanaGranja = ventanaGranja;
        posicionAnimal = 2;
        initComponents();

        Utils.setPanelEnabled(panelAtributos, false);
        Utils.setPanelEnabled(panelProductos, false);

    }

    private void cantidadAnimales() {

        try {
            error = false;
            if (Integer.parseInt(cantidadAnimalesIngresados.getText()) == 0) {
                JOptionPane.showMessageDialog(null, " Error! Debes ingresar al menos 1 animal...");
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, " Error! Ingresa un valor numérico!");
            error = true;
        }

    }

    private void definirCantidadAnimales() {

        animales = new Animal[Integer.parseInt(cantidadAnimalesIngresados.getText()) + 2];
    }

    private void crearCria() {

        animales[0] = new Animal("Vaca", 100, 2, 50, 50, 50, 50, 25, TipoAnimal.HERVIBORO, 50);
        animales[1] = new Animal("Gallina", 100, 0.5, 100, 100, 0, 0, 10, TipoAnimal.OMNIVORO, 20);
        espacioCria = Double.parseDouble(espacioAnimal.getText());
        comidaDestace = Integer.parseInt(cantidadProduccion.getText()) * (Integer.parseInt(porcentajeDestace.getText()) / 100);
        comidaSinDestace = Integer.parseInt(cantidadProduccion.getText()) * (Integer.parseInt(porcentajeSinDestace.getText()) / 100);
        materiaPrimaConDestace = Integer.parseInt(cantidadProduccion.getText()) * (Integer.parseInt(porcentajeMateriaConDestace.getText()) / 100);
        materiaPrimaSinDestace = Integer.parseInt(cantidadProduccion.getText()) * (Integer.parseInt(porcentajeMateriaSinDestace.getText()) / 100);

        edad = Integer.parseInt(edadAnimal.getText());
        precioCria = Integer.parseInt(precioAnimal.getText());

        for (int i = 0; i < 1; i++) {
            if (tipoAnimal.getItemAt(i).equals("Herbiboro")) {
                tipoCria = TipoAnimal.HERVIBORO;
            } else {
                tipoCria = TipoAnimal.OMNIVORO;
            }
        }

        animales[posicionAnimal] = new Animal(nombreAnimal.getText(), 100, espacioCria, comidaDestace, comidaSinDestace, materiaPrimaConDestace, materiaPrimaSinDestace, precioCria, tipoCria, edad);

    }

    public boolean validarDatos() {
        if ((Integer.parseInt(precioAnimal.getText()) >= 0) && (Integer.parseInt(espacioAnimal.getText()) >= 0) && (Integer.parseInt(edadAnimal.getText()) >= 0)
                && (Integer.parseInt(cantidadProduccion.getText()) >= 0) && (Integer.parseInt(porcentajeDestace.getText()) >= 0)
                && (Integer.parseInt(porcentajeSinDestace.getText()) >= 0) && (Integer.parseInt(porcentajeMateriaConDestace.getText()) >= 0)) {
            return true;
        }
        return false;
    }

    private void limpiarTexto() {
        nombreAnimal.setText("");
        precioAnimal.setText("");
        espacioAnimal.setText("");
        edadAnimal.setText("");
        cantidadProduccion.setText("");
        porcentajeDestace.setText("");
        porcentajeSinDestace.setText("");
        porcentajeMateriaConDestace.setText("");
        porcentajeMateriaSinDestace.setText("");

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jCheckBox3 = new javax.swing.JCheckBox();
        jLabel3 = new javax.swing.JLabel();
        precioPavo2 = new javax.swing.JTextField();
        etiquetaVidaPavo2 = new javax.swing.JLabel();
        vidaPavo2 = new javax.swing.JTextField();
        etiquetaEspacioPavo2 = new javax.swing.JLabel();
        espacioPavo2 = new javax.swing.JTextField();
        panelConfiguracionPartida = new javax.swing.JPanel();
        etiquetaConfiguracion = new javax.swing.JLabel();
        etiquetaAgregarAnimales = new javax.swing.JLabel();
        panelAtributos = new javax.swing.JPanel();
        etiquetaEspacioAnimal = new javax.swing.JLabel();
        espacioAnimal = new javax.swing.JTextField();
        etiquetaPrecioAnimal = new javax.swing.JLabel();
        precioAnimal = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        nombreAnimal = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        tipoAnimal = new javax.swing.JComboBox<>();
        etiquetaEdad = new javax.swing.JLabel();
        edadAnimal = new javax.swing.JTextField();
        panelProductos = new javax.swing.JPanel();
        etiquetaPorcentajeLana = new javax.swing.JLabel();
        etiqeutaPorcentajeCarneOveja = new javax.swing.JLabel();
        etiquetaCantidadP = new javax.swing.JLabel();
        cantidadProduccion = new javax.swing.JTextField();
        etiquetaPorcentajeD = new javax.swing.JLabel();
        etiquetaPorcentajeSD = new javax.swing.JLabel();
        etiquetaPorcentajeMP = new javax.swing.JLabel();
        porcentajeDestace = new javax.swing.JTextField();
        porcentajeMateriaConDestace = new javax.swing.JTextField();
        porcentajeSinDestace = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        porcentajeMateriaSinDestace = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ingresarAnimal = new javax.swing.JButton();
        panelCantidadAnimales = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        cantidadAnimalesIngresados = new javax.swing.JTextField();
        botonAceptar = new javax.swing.JButton();

        jCheckBox3.setText("Carne");

        jLabel3.setText("Precio cría");

        etiquetaVidaPavo2.setText("Vida");

        etiquetaEspacioPavo2.setText("Espacio Ocupar");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jCheckBox3)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(precioPavo2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 37, Short.MAX_VALUE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(etiquetaVidaPavo2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(vidaPavo2, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(etiquetaEspacioPavo2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(espacioPavo2)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCheckBox3)
                    .addComponent(jLabel3)
                    .addComponent(precioPavo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaVidaPavo2)
                    .addComponent(vidaPavo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaEspacioPavo2)
                    .addComponent(espacioPavo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(36, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setLocation(new java.awt.Point(0, 0));
        setResizable(false);

        panelConfiguracionPartida.setBackground(new java.awt.Color(64, 64, 64));
        panelConfiguracionPartida.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        etiquetaConfiguracion.setFont(new java.awt.Font("Ink Free", 1, 24)); // NOI18N
        etiquetaConfiguracion.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaConfiguracion.setText("Configuración de la Partida");

        etiquetaAgregarAnimales.setFont(new java.awt.Font("Ink Free", 1, 14)); // NOI18N
        etiquetaAgregarAnimales.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAgregarAnimales.setText("A g r e g a r      A n i m a l e s");

        panelAtributos.setBackground(new java.awt.Color(51, 51, 51));
        panelAtributos.setBorder(javax.swing.BorderFactory.createTitledBorder("Atributos"));
        panelAtributos.setForeground(new java.awt.Color(0, 102, 102));
        panelAtributos.setEnabled(false);

        etiquetaEspacioAnimal.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaEspacioAnimal.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaEspacioAnimal.setText("Espacio a ocupar");

        espacioAnimal.setText("12");

        etiquetaPrecioAnimal.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaPrecioAnimal.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPrecioAnimal.setText("Precio de cría");

        precioAnimal.setText("12");

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Nombre Animal");

        nombreAnimal.setText("perrito");

        jLabel2.setBackground(new java.awt.Color(255, 255, 255));
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Tipo Animal");

        tipoAnimal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Herbiboro", "Omnivoro" }));

        etiquetaEdad.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaEdad.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaEdad.setText("Edad");

        edadAnimal.setText("12");

        javax.swing.GroupLayout panelAtributosLayout = new javax.swing.GroupLayout(panelAtributos);
        panelAtributos.setLayout(panelAtributosLayout);
        panelAtributosLayout.setHorizontalGroup(
            panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtributosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAtributosLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(nombreAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(tipoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAtributosLayout.createSequentialGroup()
                        .addComponent(etiquetaPrecioAnimal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(precioAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(etiquetaEspacioAnimal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(espacioAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(28, 28, 28)
                        .addComponent(etiquetaEdad)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(edadAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        panelAtributosLayout.setVerticalGroup(
            panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtributosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(nombreAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(tipoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(70, 70, 70)
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaPrecioAnimal)
                    .addComponent(precioAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaEspacioAnimal)
                    .addComponent(espacioAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaEdad)
                    .addComponent(edadAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(76, Short.MAX_VALUE))
        );

        panelProductos.setBackground(new java.awt.Color(51, 51, 51));
        panelProductos.setBorder(javax.swing.BorderFactory.createTitledBorder("Productos"));
        panelProductos.setEnabled(false);

        etiquetaPorcentajeLana.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeLana.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeLana.setText("%");

        etiqeutaPorcentajeCarneOveja.setBackground(new java.awt.Color(255, 255, 255));
        etiqeutaPorcentajeCarneOveja.setForeground(new java.awt.Color(255, 255, 255));
        etiqeutaPorcentajeCarneOveja.setText("%");

        etiquetaCantidadP.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaCantidadP.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaCantidadP.setText("Cantidad de producción");

        cantidadProduccion.setText("12");

        etiquetaPorcentajeD.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeD.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeD.setText("Destace");

        etiquetaPorcentajeSD.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeSD.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeSD.setText("Sin destace");

        etiquetaPorcentajeMP.setBackground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeMP.setFont(new java.awt.Font("Dubai", 1, 14)); // NOI18N
        etiquetaPorcentajeMP.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPorcentajeMP.setText("Materia Prima");

        porcentajeDestace.setText("12");

        porcentajeMateriaConDestace.setText("12");

        porcentajeSinDestace.setText("12");

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setForeground(new java.awt.Color(255, 255, 255));
        jLabel15.setText("%");

        porcentajeMateriaSinDestace.setText("12");

        jLabel5.setText("%");

        jLabel6.setFont(new java.awt.Font("Dubai", 1, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Alimentos");

        javax.swing.GroupLayout panelProductosLayout = new javax.swing.GroupLayout(panelProductos);
        panelProductos.setLayout(panelProductosLayout);
        panelProductosLayout.setHorizontalGroup(
            panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelProductosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelProductosLayout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addComponent(etiquetaCantidadP)
                        .addGap(33, 33, 33)
                        .addComponent(cantidadProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelProductosLayout.createSequentialGroup()
                        .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(etiquetaPorcentajeSD)
                            .addComponent(etiquetaPorcentajeD))
                        .addGap(49, 49, 49)
                        .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(porcentajeDestace, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(porcentajeSinDestace, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelProductosLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(etiquetaPorcentajeLana)
                                    .addComponent(etiqeutaPorcentajeCarneOveja)))
                            .addGroup(panelProductosLayout.createSequentialGroup()
                                .addGap(110, 110, 110)
                                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(porcentajeMateriaSinDestace, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(porcentajeMateriaConDestace, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(jLabel5))))))
                .addContainerGap(42, Short.MAX_VALUE))
            .addGroup(panelProductosLayout.createSequentialGroup()
                .addGap(103, 103, 103)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(etiquetaPorcentajeMP)
                .addGap(52, 52, 52))
        );
        panelProductosLayout.setVerticalGroup(
            panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelProductosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cantidadProduccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaCantidadP))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 39, Short.MAX_VALUE)
                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(etiquetaPorcentajeMP, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelProductosLayout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(porcentajeMateriaConDestace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelProductosLayout.createSequentialGroup()
                        .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(etiquetaPorcentajeLana)
                            .addComponent(porcentajeDestace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaPorcentajeD))
                        .addGap(33, 33, 33)))
                .addGroup(panelProductosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaPorcentajeSD)
                    .addComponent(etiqeutaPorcentajeCarneOveja)
                    .addComponent(porcentajeSinDestace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(porcentajeMateriaSinDestace, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addContainerGap())
        );

        ingresarAnimal.setBackground(new java.awt.Color(51, 51, 51));
        ingresarAnimal.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        ingresarAnimal.setForeground(new java.awt.Color(255, 255, 255));
        ingresarAnimal.setText("I N G R E S A R    A N I M A L");
        ingresarAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingresarAnimalActionPerformed(evt);
            }
        });

        panelCantidadAnimales.setBackground(new java.awt.Color(51, 51, 51));
        panelCantidadAnimales.setBorder(javax.swing.BorderFactory.createTitledBorder("Animales para el mercado"));

        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Cantidad de animales");

        botonAceptar.setText("ACEPTAR");
        botonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCantidadAnimalesLayout = new javax.swing.GroupLayout(panelCantidadAnimales);
        panelCantidadAnimales.setLayout(panelCantidadAnimalesLayout);
        panelCantidadAnimalesLayout.setHorizontalGroup(
            panelCantidadAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCantidadAnimalesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cantidadAnimalesIngresados, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelCantidadAnimalesLayout.setVerticalGroup(
            panelCantidadAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCantidadAnimalesLayout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(panelCantidadAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(cantidadAnimalesIngresados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonAceptar))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelConfiguracionPartidaLayout = new javax.swing.GroupLayout(panelConfiguracionPartida);
        panelConfiguracionPartida.setLayout(panelConfiguracionPartidaLayout);
        panelConfiguracionPartidaLayout.setHorizontalGroup(
            panelConfiguracionPartidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfiguracionPartidaLayout.createSequentialGroup()
                .addGap(355, 355, 355)
                .addComponent(ingresarAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConfiguracionPartidaLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(panelConfiguracionPartidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConfiguracionPartidaLayout.createSequentialGroup()
                        .addComponent(etiquetaConfiguracion, javax.swing.GroupLayout.PREFERRED_SIZE, 413, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(194, 194, 194))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelConfiguracionPartidaLayout.createSequentialGroup()
                        .addComponent(etiquetaAgregarAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 206, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(354, 354, 354))))
            .addGroup(panelConfiguracionPartidaLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(panelConfiguracionPartidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelCantidadAnimales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelConfiguracionPartidaLayout.createSequentialGroup()
                        .addComponent(panelAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(panelProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(60, Short.MAX_VALUE))
        );
        panelConfiguracionPartidaLayout.setVerticalGroup(
            panelConfiguracionPartidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConfiguracionPartidaLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(etiquetaConfiguracion)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(etiquetaAgregarAnimales)
                .addGap(18, 18, 18)
                .addComponent(panelCantidadAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelConfiguracionPartidaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(panelAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ingresarAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelConfiguracionPartida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelConfiguracionPartida, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //botón, agregamos a un animal al mercado para después comprarlo

    private void ingresarAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingresarAnimalActionPerformed

        try {
            try {
                crearCria();
                if (((Integer.parseInt(porcentajeDestace.getText()) + Integer.parseInt(porcentajeMateriaConDestace.getText())) > 100)
                        || (Integer.parseInt(porcentajeSinDestace.getText()) + Integer.parseInt(porcentajeMateriaSinDestace.getText()) > 100)) {
                    JOptionPane.showMessageDialog(null, "Los productos están mal distribuidos, su suma debe ser menor o igual a 100%");
                } else if (!validarDatos()) {
                    JOptionPane.showMessageDialog(null, "Los valores deben ser mayor a 0");
                } else {
                    JOptionPane.showMessageDialog(null, "Animal " + getAnimales()[posicionAnimal].getNombreAnimal() + " ingresado!");
                    limpiarTexto();
                    posicionAnimal++;
                }

                if (posicionAnimal == animales.length) {
                    this.ventanaGranja.configurarPlantas();
                    this.setVisible(false);
                    ventanaGranja.establecerAnimalesComprados(animales.length);
                }

            } catch (NumberFormatException | NullPointerException ae) {
                JOptionPane.showMessageDialog(null, "No has rellenado todos los atributos!");
            }
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(null, "No se aceptan más animales!");
        }


    }//GEN-LAST:event_ingresarAnimalActionPerformed

    private void botonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarActionPerformed
        cantidadAnimales();
        if (error == false && Integer.parseInt(cantidadAnimalesIngresados.getText()) > 0) {
            definirCantidadAnimales();
            Utils.setPanelEnabled(panelAtributos, true);
            Utils.setPanelEnabled(panelProductos, true);
            Utils.setPanelEnabled(panelCantidadAnimales, false);
        } else {
            JOptionPane.showMessageDialog(null, "Debes ingresar un valor mayor a 0");
        }

    }//GEN-LAST:event_botonAceptarActionPerformed

    private void verificarProductos() {

    }

    public Animal[] getAnimales() {
        return animales;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAceptar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JTextField cantidadAnimalesIngresados;
    private javax.swing.JTextField cantidadProduccion;
    private javax.swing.JTextField edadAnimal;
    private javax.swing.JTextField espacioAnimal;
    private javax.swing.JTextField espacioPavo2;
    private javax.swing.JLabel etiqeutaPorcentajeCarneOveja;
    private javax.swing.JLabel etiquetaAgregarAnimales;
    private javax.swing.JLabel etiquetaCantidadP;
    private javax.swing.JLabel etiquetaConfiguracion;
    private javax.swing.JLabel etiquetaEdad;
    private javax.swing.JLabel etiquetaEspacioAnimal;
    private javax.swing.JLabel etiquetaEspacioPavo2;
    private javax.swing.JLabel etiquetaPorcentajeD;
    private javax.swing.JLabel etiquetaPorcentajeLana;
    private javax.swing.JLabel etiquetaPorcentajeMP;
    private javax.swing.JLabel etiquetaPorcentajeSD;
    private javax.swing.JLabel etiquetaPrecioAnimal;
    private javax.swing.JLabel etiquetaVidaPavo2;
    private javax.swing.JButton ingresarAnimal;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JTextField nombreAnimal;
    private javax.swing.JPanel panelAtributos;
    private javax.swing.JPanel panelCantidadAnimales;
    private javax.swing.JPanel panelConfiguracionPartida;
    private javax.swing.JPanel panelProductos;
    private javax.swing.JTextField porcentajeDestace;
    private javax.swing.JTextField porcentajeMateriaConDestace;
    private javax.swing.JTextField porcentajeMateriaSinDestace;
    private javax.swing.JTextField porcentajeSinDestace;
    private javax.swing.JTextField precioAnimal;
    private javax.swing.JTextField precioPavo2;
    private javax.swing.JComboBox<String> tipoAnimal;
    private javax.swing.JTextField vidaPavo2;
    // End of variables declaration//GEN-END:variables

}
