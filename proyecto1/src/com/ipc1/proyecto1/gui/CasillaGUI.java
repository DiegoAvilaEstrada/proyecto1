package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.Suelo;
import com.ipc1.proyecto1.logica.SueloAgua;
import com.ipc1.proyecto1.logica.SueloDesierto;
import com.ipc1.proyecto1.logica.SueloGrama;
import com.ipc1.proyecto1.logica.TipoPlanta;
import com.ipc1.proyecto1.recursos.imagenes.ImageLoader;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class CasillaGUI<T extends Suelo> extends JPanel {

    public final static int TAMANO_CASILLA = 115;

    private boolean hiloSembrarVivo;

    public boolean isHiloSembrarVivo() {
        return hiloSembrarVivo;
    }
    private T suelo;
    private final int fila;
    private final int columna;
    private final GranjaGUI parent;
    private final int numeroCasilla;
    private Image backgrountImage;

    public CasillaGUI(T suelo, GranjaGUI parent) {
        this.parent = parent;
        this.numeroCasilla = suelo.getNumero();
        this.fila = suelo.getFila();
        this.columna = suelo.getColumna();
        this.iniciarComponentes(suelo);
        addMouseListener(new CasillaMouseActionListener());

    }

    public CasillaGUI(int fila, int columna, int numeroCasilla, GranjaGUI parent) {
        this.parent = parent;
        this.numeroCasilla = numeroCasilla;
        this.fila = fila;
        this.columna = columna;
        this.iniciarComponentes(null);
        addMouseListener(new CasillaMouseActionListener());
    }

    public void iniciarComponentes(T suelo) {
        this.removeAll();
        this.suelo = suelo;
        this.setBackgroundImage();
        this.setBounds(TAMANO_CASILLA * columna, TAMANO_CASILLA * fila, TAMANO_CASILLA, TAMANO_CASILLA);
        this.setBorder(javax.swing.BorderFactory.createTitledBorder("" + numeroCasilla));
        this.repaint();
    }

    public Suelo getSuelo() {
        return suelo;
    }

    public int getFila() {
        return fila;
    }

    public int getColumna() {
        return columna;
    }

    public int getNumeroCasilla() {
        return numeroCasilla;
    }

    private void setBackgroundImage() {
        if (suelo == null) {
            return;
        }
        try {
            backgrountImage = ImageIO.read(ImageLoader.getImagePath(suelo.getTipoSuelo().imagen().obtenerImagen()));
        } catch (IOException ex) {

        }
    }

    public void setBackgroundImage(ImagesEnum imagesEnum) {
        try {
            backgrountImage = ImageIO.read(ImageLoader.getImagePath(imagesEnum.obtenerImagen()));
        } catch (IOException ex) {
            Logger.getLogger(CasillaGUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        this.repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(backgrountImage, 0, 0, null);
    }

    class CasillaMouseActionListener implements MouseListener { //esta clase es para darle la opcion de clicks a nuestras casillas

        @Override
        public void mouseClicked(MouseEvent e) {
            if (CasillaGUI.this.getSuelo() instanceof SueloGrama) {

                boolean encontrado = parent.getGranja().existeEnOtraParcela((SueloGrama) CasillaGUI.this.getSuelo());
                if (encontrado) {
                    parent.accederVentanaMercadoAnimales(CasillaGUI.this);
                } else {
                    SueloGramaGUI sueloGramaGUI = new SueloGramaGUI(CasillaGUI.this, parent);
                    sueloGramaGUI.setVisible(true);
                }

            } else if (CasillaGUI.this.getSuelo() instanceof SueloAgua) {
                SueloAguaGUI sueloAguaGUI = new SueloAguaGUI(CasillaGUI.this, parent);
                sueloAguaGUI.setVisible(true);
            } else if (CasillaGUI.this.getSuelo() instanceof SueloDesierto) {
                JOptionPane.showMessageDialog(null, "En el desierto no se hace nada!");
            } else {
                CasillaDisponible casillaDisponible = new CasillaDisponible(CasillaGUI.this, parent);
                casillaDisponible.setVisible(true);
            }

        }

        @Override
        public void mousePressed(MouseEvent e) {

        }

        @Override
        public void mouseReleased(MouseEvent e) {

        }

        @Override
        public void mouseEntered(MouseEvent e) {

        }

        @Override
        public void mouseExited(MouseEvent e) {

        }

    }

    public void iniciarVidaGrama() { //Este hilo es para la siembra y cosecha de una planta
        Thread hiloSiemnbra = new Thread() {
            @Override
            public void run() {

                try {
                    iniciarVidaGrama();
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }

            }

            void iniciarVidaGrama() throws InterruptedException {
                SueloGrama sueloGrama = (SueloGrama) suelo;
                CasillaGUI.this.setBackgroundImage(ImagesEnum.CAMPO_SEMBRADO);

                while (!sueloGrama.esTiempoParaCosechar() && sueloGrama.getSemilla() != null && GranjaGUI.cerrarTodo == false) {
                    Thread.sleep(1000);
                    sueloGrama.envejecerSuelo();
                    System.out.println("casilla esperando tiempo para cosechar, casilla:" + sueloGrama.getNumero());
                }

                CasillaGUI.this.establecerImagenCosecha();

                System.out.println("tiempo para cosechar " + suelo.getNumero());
                while (!sueloGrama.esTiempoPodrirse() && sueloGrama.getSemilla() != null && GranjaGUI.cerrarTodo == false) {
                    Thread.sleep(1000);
                    sueloGrama.envejecerSuelo();
                    System.out.println("casilla esperando tiempo para podrirse casilla:" + sueloGrama.getNumero());
                }
                if (sueloGrama.getSemilla() == null) {
                    CasillaGUI.this.setBackgroundImage(ImagesEnum.GRAMA);
                } else {
                    CasillaGUI.this.setBackgroundImage(ImagesEnum.COSECHA_PODRIDA);
                    
                }

            }

        };
        hiloSiemnbra.start();
    }
   
    public void establecerImagenCosecha() { //Generamos una imagen dependiendo de la planta que hemos sembrado
        if (((SueloGrama) this.getSuelo()).getSemilla().getNombrePlanta().equals("Maíz")) {
            this.setBackgroundImage(ImagesEnum.MAIZ);
        } else if (((SueloGrama) this.getSuelo()).getSemilla().getNombrePlanta().equals("Manzano")) {
            this.setBackgroundImage(ImagesEnum.MANZANO);
        } else if (((SueloGrama) this.getSuelo()).getSemilla().getTipoProduccion() == TipoPlanta.FRUTA) {
            this.setBackgroundImage(ImagesEnum.FRUTA);
        } else if (((SueloGrama) this.getSuelo()).getSemilla().getTipoProduccion() == TipoPlanta.GRANO) {
            this.setBackgroundImage(ImagesEnum.GRANO);
        }
    }
    
    public void iniciarPesca(){
    
    
    }
    
    
}
