package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Suelo;
import com.ipc1.proyecto1.logica.TipoSuelo;
import com.ipc1.proyecto1.recursos.imagenes.ImageLoader;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class CasillaDisponible extends javax.swing.JFrame {

    private CasillaGUI casillaGUI;
    private GranjaGUI ventanaGranja;
    private final int PRECIO_CASILLA = 25;
    public static int casillaComprada = 0;
    public static int cantidadCasillasCompradas = 0;

    public CasillaDisponible(CasillaGUI casillaGUI, GranjaGUI ventanaGranja) {
        this.ventanaGranja = ventanaGranja;
        this.casillaGUI = casillaGUI;

        initComponents();
    }

    public void comprarCasilla() {
        if (ventanaGranja.getGranjero().getOro() > PRECIO_CASILLA) {
            Suelo suelo = ventanaGranja.getGranja().generarCasillaAleatoria(this.casillaGUI.getFila(), this.casillaGUI.getColumna(), this.casillaGUI.getNumeroCasilla());
            casillaGUI.iniciarComponentes(suelo);
            ventanaGranja.getGranjero().gastarOro(PRECIO_CASILLA);
            ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
            this.setVisible(false);
            this.ventanaGranja.repaint();
            cantidadCasillasCompradas++;
        } else {
            JOptionPane.showMessageDialog(null, ventanaGranja.getGranjero().getNickName() + ", no tienes más monedas para comprar");
        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        botonComprarCasilla = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("TERRENO DISPONIBLE");

        jLabel2.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("PRECIO: 25 MONEDAS DE ORO");

        botonComprarCasilla.setFont(new java.awt.Font("Dialog", 1, 14)); // NOI18N
        botonComprarCasilla.setText("C O M P R A R");
        botonComprarCasilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarCasillaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(123, 123, 123)
                            .addComponent(botonComprarCasilla, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addGap(93, 93, 93)
                            .addComponent(jLabel2))))
                .addContainerGap(100, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(49, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56)
                .addComponent(botonComprarCasilla, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComprarCasillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarCasillaActionPerformed
        comprarCasilla();

    }//GEN-LAST:event_botonComprarCasillaActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonComprarCasilla;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
