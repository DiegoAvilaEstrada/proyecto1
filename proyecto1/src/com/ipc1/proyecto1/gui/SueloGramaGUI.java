package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Granja;
import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.Planta;
import com.ipc1.proyecto1.logica.SueloGrama;
import com.ipc1.proyecto1.logica.TipoPlanta;
import com.ipc1.proyecto1.logica.Utils;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

public class SueloGramaGUI extends javax.swing.JFrame {

    private final CasillaGUI casillaGUI;
    private GranjaGUI ventanaGranja;
    //Atributos de mi semilla
    private TipoPlanta tipoProduccion = null;
    private String nombrePlanta = null;
    private int cantidadSemillas = 0;
    private int precioSemilla = 0;
    private int tiempoCosecha = 0;
    private int tiempoPodrirse = 0;
    private int alimentoProducido = 0;
    //______________________________
    private final int PRECIO_LIMPIEZA_TERRENO = 5;

    private Granja granja;

    public SueloGramaGUI(CasillaGUI casillaGUI, GranjaGUI ventanaGranja) {
        this.casillaGUI = casillaGUI;
        this.ventanaGranja = ventanaGranja;
        granja = new Granja();
        initComponents();
        iniciarComponentes();

    }

    public void iniciarComponentes() {
        for (Planta planta : ventanaGranja.getConfiguracionPlanta().getPlantas()) {
            comboSemillasCompradas.addItem(planta.getNombrePlanta());
        }
    }

    public void verSemillas() {

        int tamanio = ventanaGranja.getGranjero().getSemillas().length;
        JOptionPane.showMessageDialog(null, "tamaño arreglo " + tamanio);
    }

    public void eliminarSemillas(int cantidadSemillas, int tamanioArreglo, String nombrePlanta) {
        int indice = 0;

        while (indice < tamanioArreglo) {
            if (comboSemillasCompradas.getItemAt(indice).equals(nombrePlanta)) {
                for (int i = indice; i < tamanioArreglo - 1; i++) {

                }
                tamanioArreglo--;
            } else {
                indice++;
            }
        }

    }

    public void llenarAtributosSemilla(int indice) {
        tipoProduccion = ventanaGranja.getMercado().getSemillas()[indice].getTipoProduccion();
        nombrePlanta = ventanaGranja.getMercado().getSemillas()[indice].getNombrePlanta();
        cantidadSemillas = ventanaGranja.getMercado().getSemillas()[indice].getCantidadSemillas();
        precioSemilla = ventanaGranja.getMercado().getSemillas()[indice].getPrecioSemilla();
        tiempoCosecha = ventanaGranja.getMercado().getSemillas()[indice].getTiempoCosecha();
        tiempoPodrirse = ventanaGranja.getMercado().getSemillas()[indice].getTiempoPodrirse();
        alimentoProducido = ventanaGranja.getMercado().getSemillas()[indice].getAlimentoProducido();

    }

    public void obtenerSemilla() {
        ((SueloGrama) casillaGUI.getSuelo()).Sembrar(new Planta(tipoProduccion, nombrePlanta, cantidadSemillas, precioSemilla, tiempoCosecha, tiempoPodrirse, alimentoProducido));
        casillaGUI.iniciarVidaGrama();
        Utils.setPanelEnabled(panelPlantas, false);
        //this.setVisible(false);
        JOptionPane.showMessageDialog(null, "Se ha sembrado la planta de: " + nombrePlanta);

    }

    public void sembrarSemilla() {
        try { //try catch que me sirve para que no agrege otra semilla a un campo que ya tiene sembrada una semilla
            try { // try catch que me sirve para ver si hay o no una siembra en un campo
                int indice;

                indice = comboSemillasCompradas.getSelectedIndex();

                for (int i = 0; i < ventanaGranja.getMercado().getSemillas().length; i++) {
                    if (comboSemillasCompradas.getItemAt(indice).equals(ventanaGranja.getMercado().getSemillas()[i].getNombrePlanta())) { //comparamos cual semilla hemos elegido
                        llenarAtributosSemilla(i);
                        if (ventanaGranja.getConfiguracionPlanta().getContadorSemillas()[indice] < cantidadSemillas) { //vemos si hay o no semillas disponibles para sembrar
                            JOptionPane.showMessageDialog(null, "No hay suficientes semillas, necesitas " + cantidadSemillas + " semillas para sembrar!");
                        } else {// AQUI YA SE SIEMBRA MI SEMILLA
                            obtenerSemilla();
                            ventanaGranja.getSemillasSembradas()[indice]++;
                            restarCantidadSemillas(cantidadSemillas, indice);
                            this.setVisible(false);
                        }
                    }
                }

            } catch (NullPointerException ea) {
                JOptionPane.showMessageDialog(null, "No hay ninguna semilla disponible");
            }
        } catch (ArrayIndexOutOfBoundsException ae) {
            JOptionPane.showMessageDialog(null, "No se puede sembrar más, ya hay una siembra en este suelo");
        }
    }

    public void cosecharPlanta() {

        ventanaGranja.getGranja().entrarBodega().recogerAlimentoPlanta(((SueloGrama) casillaGUI.getSuelo()).getSemilla().getAlimentoProducido());
        GranjaGUI.alimentoGenerado += ((SueloGrama) casillaGUI.getSuelo()).getSemilla().getAlimentoProducido();
        JOptionPane.showMessageDialog(null, "Cosecha Recogida!");
        Utils.setPanelEnabled(panelPlantas, true);
        //Utils.setPanelEnabled(panelCosecha, false);
        ((SueloGrama) casillaGUI.getSuelo()).iniciarSuelo();
    }

    public void restarCantidadSemillas(int cantidad, int indice) {
        ventanaGranja.getConfiguracionPlanta().getContadorSemillas()[indice] -= cantidad;
        ventanaGranja.getSemillasCompradas()[indice].setText("" + ventanaGranja.getConfiguracionPlanta().getContadorSemillas()[indice]);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelPlantas = new javax.swing.JPanel();
        botonSembrar = new javax.swing.JButton();
        comboSemillasCompradas = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        botonCosechar = new javax.swing.JButton();
        botonComprarSemilla = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        botonLimpiarTerreno = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        panelPlantas.setBackground(new java.awt.Color(51, 51, 51));
        panelPlantas.setBorder(javax.swing.BorderFactory.createTitledBorder("Sembrar Semillas"));

        botonSembrar.setText("Sembrar");
        botonSembrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonSembrarActionPerformed(evt);
            }
        });

        comboSemillasCompradas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboSemillasCompradasActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Semillas Compradas");

        javax.swing.GroupLayout panelPlantasLayout = new javax.swing.GroupLayout(panelPlantas);
        panelPlantas.setLayout(panelPlantasLayout);
        panelPlantasLayout.setHorizontalGroup(
            panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelPlantasLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 28, Short.MAX_VALUE)
                .addComponent(comboSemillasCompradas, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonSembrar)
                .addContainerGap())
        );
        panelPlantasLayout.setVerticalGroup(
            panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPlantasLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelPlantasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonSembrar)
                    .addComponent(comboSemillasCompradas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Otras Optiones"));

        botonCosechar.setText("C O S E C H A R");
        botonCosechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCosecharActionPerformed(evt);
            }
        });

        botonComprarSemilla.setText("COMPRAR SEMILLAS");
        botonComprarSemilla.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarSemillaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(43, 43, 43)
                .addComponent(botonComprarSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonCosechar, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonCosechar)
                    .addComponent(botonComprarSemilla))
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(51, 51, 51));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Limpiar terreno"));

        botonLimpiarTerreno.setText("LIMPIAR  TERRENO");
        botonLimpiarTerreno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonLimpiarTerrenoActionPerformed(evt);
            }
        });

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Precio de Limpieza: 5 monedas");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonLimpiarTerreno, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(botonLimpiarTerreno))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(panelPlantas, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panelPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(142, 142, 142))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonSembrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonSembrarActionPerformed
        if (((SueloGrama) casillaGUI.getSuelo()).getSemilla() == null) {
            sembrarSemilla();
        } else {
            JOptionPane.showMessageDialog(null, "Hay una siembra");
        }

    }//GEN-LAST:event_botonSembrarActionPerformed

    private void botonComprarSemillaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarSemillaActionPerformed
        ventanaGranja.accederVentanaMercadoPlantas(comboSemillasCompradas);
    }//GEN-LAST:event_botonComprarSemillaActionPerformed

    private void botonLimpiarTerrenoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonLimpiarTerrenoActionPerformed
        if (((SueloGrama) casillaGUI.getSuelo()).esTiempoParaCosechar()) {
            SueloGrama sueloGrama = new SueloGrama(casillaGUI.getSuelo().getFila(), casillaGUI.getSuelo().getColumna(), casillaGUI.getSuelo().getNumero());
            ventanaGranja.getGranjero().gastarOro(PRECIO_LIMPIEZA_TERRENO);
            ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
            casillaGUI.iniciarComponentes(sueloGrama);
            this.setVisible(false);
        } else {
            JOptionPane.showMessageDialog(null, "No hay ninguna siembra podrida!");
        }


    }//GEN-LAST:event_botonLimpiarTerrenoActionPerformed

    private void comboSemillasCompradasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboSemillasCompradasActionPerformed

    }//GEN-LAST:event_comboSemillasCompradasActionPerformed

    private void botonCosecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCosecharActionPerformed
        try {
            if (((SueloGrama) casillaGUI.getSuelo()).esTiempoParaCosechar() && !((SueloGrama) casillaGUI.getSuelo()).esTiempoPodrirse()) {
                cosecharPlanta();
                Utils.setPanelEnabled(panelPlantas, true);
            } else {
                JOptionPane.showMessageDialog(null, "No se puede cosechar");
            }

        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(null, "No hay ninguna siembra");
        }


    }//GEN-LAST:event_botonCosecharActionPerformed

    public void setComboSemillasCompradas(JComboBox<String> comboSemillasCompradas) {
        this.comboSemillasCompradas = comboSemillasCompradas;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonComprarSemilla;
    private javax.swing.JButton botonCosechar;
    private javax.swing.JButton botonLimpiarTerreno;
    private javax.swing.JButton botonSembrar;
    private javax.swing.JComboBox<String> comboSemillasCompradas;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel panelPlantas;
    // End of variables declaration//GEN-END:variables
}
