package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Planta;
import com.ipc1.proyecto1.logica.TipoPlanta;
import com.ipc1.proyecto1.logica.Utils;
import javax.swing.JOptionPane;

public class ConfiguracionPlantaGUI extends javax.swing.JFrame {
    
    private Planta plantas[];
    private Planta semillas;
    private int posicionPlanta;
    private int cantidadPlantas;
    private TipoPlanta tipoProduccion;
    private GranjaGUI ventanaGranja;
    private boolean error;
    private int contadorSemillas[];
    
    public ConfiguracionPlantaGUI(GranjaGUI ventanaGranja) {
        this.ventanaGranja = ventanaGranja;
        posicionPlanta = 2;
        initComponents();
        Utils.setPanelEnabled(panelCantidad, true);
        Utils.setPanelEnabled(panelAtributos, false);
    }
    
    private void cantidadPlantas() {
        
        try {
            error = false;
            if (Integer.parseInt(cantidadPlantasIngresadas.getText()) == 0) {
                JOptionPane.showMessageDialog(null, "Error, Debes ingresar al menos una planta");
            }
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, " Error! Ingresa un valor numérico!");
            error = true;
        }
        
    }
    
    public void definirCantidadPlantas() {
        plantas = new Planta[Integer.parseInt(cantidadPlantasIngresadas.getText()) + 2];
        contadorSemillas = new int[plantas.length];
        
    }
    
    private void crearSemilla() {  //Aquí creamos nuestras semillas que le enviaremos a nuestro mercado

        for (int i = 0; i < 1; i++) {
            if (tipoPlanta.getItemAt(i).equals("Grano")) {
                tipoProduccion = TipoPlanta.GRANO;
            } else {
                tipoProduccion = TipoPlanta.FRUTA;
            }
        }
        plantas[0] = new Planta(TipoPlanta.GRANO, "Maíz", 2, 5, 15, 20, 25);
        plantas[1] = new Planta(TipoPlanta.FRUTA, "Manzano", 2, 5, 15, 20, 25);
        contadorSemillas[0] = 0;
        contadorSemillas[1] = 0;
        plantas[posicionPlanta] = new Planta(tipoProduccion, nombrePlanta.getText(), Integer.parseInt(cantidadSemillas.getText()), Integer.parseInt(precioSemilla.getText()), Integer.parseInt(tiempoCosecha.getText()), Integer.parseInt(tiempoPodrirse.getText()), Integer.parseInt(cantidadAlimento.getText()));
        contadorSemillas[posicionPlanta] = 0;
    }
    
    public boolean validarDatos() {
        if ((Integer.parseInt(tiempoCosecha.getText()) >= 0) && (Integer.parseInt(tiempoPodrirse.getText()) >= 0) && (Integer.parseInt(precioSemilla.getText()) >= 0)
                && (Integer.parseInt(cantidadSemillas.getText()) >= 0) && (Integer.parseInt(cantidadAlimento.getText()) >= 0)) {
            return true;
        }
        return false;
    }
    
    private void limpiarTexto() {
        nombrePlanta.setText("");
        tiempoCosecha.setText("");
        tiempoPodrirse.setText("");
        precioSemilla.setText("");
        cantidadSemillas.setText("");
        cantidadAlimento.setText("");
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        etiquetaConfiPlanta = new javax.swing.JLabel();
        panelAtributos = new javax.swing.JPanel();
        etiquetaCantidadSemilla = new javax.swing.JLabel();
        cantidadSemillas = new javax.swing.JTextField();
        etiquetaPrecioFrijol = new javax.swing.JLabel();
        precioSemilla = new javax.swing.JTextField();
        etiquetaTiempoCosecha = new javax.swing.JLabel();
        tiempoCosecha = new javax.swing.JTextField();
        etiquetaTiempoPodrirse = new javax.swing.JLabel();
        tiempoPodrirse = new javax.swing.JTextField();
        etiquetaAlimentoProducido = new javax.swing.JLabel();
        cantidadAlimento = new javax.swing.JTextField();
        etiquetaNombrePlanta = new javax.swing.JLabel();
        nombrePlanta = new javax.swing.JTextField();
        etiquetaTipoPlanta = new javax.swing.JLabel();
        tipoPlanta = new javax.swing.JComboBox<>();
        botonIngresarPlanta = new javax.swing.JButton();
        panelCantidad = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        cantidadPlantasIngresadas = new javax.swing.JTextField();
        botonAceptar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(64, 64, 64));

        etiquetaConfiPlanta.setFont(new java.awt.Font("Ink Free", 1, 18)); // NOI18N
        etiquetaConfiPlanta.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaConfiPlanta.setText("A G R E G A R       P L A N T A S");

        panelAtributos.setBackground(new java.awt.Color(51, 51, 51));
        panelAtributos.setBorder(javax.swing.BorderFactory.createTitledBorder("Atributos"));
        panelAtributos.setEnabled(false);

        etiquetaCantidadSemilla.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaCantidadSemilla.setText("Cantidad semillas necesarias");

        cantidadSemillas.setText("12");

        etiquetaPrecioFrijol.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaPrecioFrijol.setText("precio Semilla");

        precioSemilla.setText("12");

        etiquetaTiempoCosecha.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaTiempoCosecha.setText("tiempo de Cosecha (seg)");

        tiempoCosecha.setText("12");

        etiquetaTiempoPodrirse.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaTiempoPodrirse.setText("tiempo en podrirse (seg)");

        tiempoPodrirse.setText("12");

        etiquetaAlimentoProducido.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoProducido.setText("Alimento Producido");

        cantidadAlimento.setText("12");

        etiquetaNombrePlanta.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaNombrePlanta.setText("Nombre de Planta:");

        nombrePlanta.setText("flor");

        etiquetaTipoPlanta.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaTipoPlanta.setText("Tipo Planta");

        tipoPlanta.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Grano", "Fruta" }));

        javax.swing.GroupLayout panelAtributosLayout = new javax.swing.GroupLayout(panelAtributos);
        panelAtributos.setLayout(panelAtributosLayout);
        panelAtributosLayout.setHorizontalGroup(
            panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtributosLayout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAtributosLayout.createSequentialGroup()
                        .addComponent(etiquetaTiempoCosecha)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(tiempoCosecha, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(etiquetaPrecioFrijol)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(precioSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(etiquetaCantidadSemilla)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cantidadSemillas, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAtributosLayout.createSequentialGroup()
                        .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelAtributosLayout.createSequentialGroup()
                                .addComponent(etiquetaTiempoPodrirse)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(tiempoPodrirse, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(etiquetaAlimentoProducido)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cantidadAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelAtributosLayout.createSequentialGroup()
                                .addComponent(etiquetaNombrePlanta)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(nombrePlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(51, 51, 51)
                                .addComponent(etiquetaTipoPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(3, 3, 3)
                                .addComponent(tipoPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        panelAtributosLayout.setVerticalGroup(
            panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAtributosLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaNombrePlanta)
                    .addComponent(nombrePlanta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaTipoPlanta)
                    .addComponent(tipoPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaTiempoCosecha)
                    .addComponent(tiempoCosecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaPrecioFrijol)
                    .addComponent(precioSemilla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cantidadSemillas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaCantidadSemilla))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(panelAtributosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(etiquetaTiempoPodrirse)
                    .addComponent(tiempoPodrirse, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etiquetaAlimentoProducido)
                    .addComponent(cantidadAlimento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        botonIngresarPlanta.setBackground(new java.awt.Color(51, 51, 51));
        botonIngresarPlanta.setForeground(new java.awt.Color(255, 255, 255));
        botonIngresarPlanta.setText("I N G R E S A R    P L A N T A");
        botonIngresarPlanta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIngresarPlantaActionPerformed(evt);
            }
        });

        panelCantidad.setBackground(new java.awt.Color(51, 51, 51));
        panelCantidad.setBorder(javax.swing.BorderFactory.createTitledBorder("Plantas para Mercado"));

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Cantidad");

        botonAceptar.setText("ACEPTAR");
        botonAceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCantidadLayout = new javax.swing.GroupLayout(panelCantidad);
        panelCantidad.setLayout(panelCantidadLayout);
        panelCantidadLayout.setHorizontalGroup(
            panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCantidadLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cantidadPlantasIngresadas, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(110, 110, 110)
                .addComponent(botonAceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(84, Short.MAX_VALUE))
        );
        panelCantidadLayout.setVerticalGroup(
            panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCantidadLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(cantidadPlantasIngresadas, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonAceptar))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(202, 202, 202)
                        .addComponent(botonIngresarPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(167, 167, 167)
                        .addComponent(etiquetaConfiPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 313, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(206, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelAtributos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCantidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(27, 27, 27))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(etiquetaConfiPlanta)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panelCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panelAtributos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonIngresarPlanta, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonIngresarPlantaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIngresarPlantaActionPerformed
        try {
            if (!validarDatos()) {
                JOptionPane.showMessageDialog(null, "Los valores deben ser mayores a 0");
            } else {
                crearSemilla();
                JOptionPane.showMessageDialog(null, "Has ingresado a la planta:  " + plantas[posicionPlanta].getNombrePlanta());
                limpiarTexto();
                posicionPlanta++;
                if (posicionPlanta == plantas.length) {
                    this.ventanaGranja.iniciarComponentes();
                    this.ventanaGranja.establecerSemillasAcumuladas(plantas.length);
                    this.ventanaGranja.establecerSemillasSembradas(plantas.length);
                    this.setVisible(false);
                }
            }
            
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException | NullPointerException e) {
            JOptionPane.showMessageDialog(null, "No se aceptan más semillas! ó no has rellenado todos los atributos");
        }

    }//GEN-LAST:event_botonIngresarPlantaActionPerformed

    private void botonAceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarActionPerformed
        cantidadPlantas();
        if (error == false && Integer.parseInt(cantidadPlantasIngresadas.getText()) > 0) {
            definirCantidadPlantas();
            Utils.setPanelEnabled(panelAtributos, true);
            Utils.setPanelEnabled(panelCantidad, false);
        } else {
            JOptionPane.showMessageDialog(null, "Debes ingresar un valor mayor a 0");
        }
        

    }//GEN-LAST:event_botonAceptarActionPerformed
    
    public Planta[] getPlantas() {
        return plantas;
    }
    
    public int[] getContadorSemillas() {
        return contadorSemillas;
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAceptar;
    private javax.swing.JButton botonIngresarPlanta;
    private javax.swing.JTextField cantidadAlimento;
    private javax.swing.JTextField cantidadPlantasIngresadas;
    private javax.swing.JTextField cantidadSemillas;
    private javax.swing.JLabel etiquetaAlimentoProducido;
    private javax.swing.JLabel etiquetaCantidadSemilla;
    private javax.swing.JLabel etiquetaConfiPlanta;
    private javax.swing.JLabel etiquetaNombrePlanta;
    private javax.swing.JLabel etiquetaPrecioFrijol;
    private javax.swing.JLabel etiquetaTiempoCosecha;
    private javax.swing.JLabel etiquetaTiempoPodrirse;
    private javax.swing.JLabel etiquetaTipoPlanta;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField nombrePlanta;
    private javax.swing.JPanel panelAtributos;
    private javax.swing.JPanel panelCantidad;
    private javax.swing.JTextField precioSemilla;
    private javax.swing.JTextField tiempoCosecha;
    private javax.swing.JTextField tiempoPodrirse;
    private javax.swing.JComboBox<String> tipoPlanta;
    // End of variables declaration//GEN-END:variables
}
