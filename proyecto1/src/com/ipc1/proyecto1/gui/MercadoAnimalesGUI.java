package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.AlimentoAnimalHerbiboro;
import com.ipc1.proyecto1.logica.AlimentoAnimalOmnivoro;
import com.ipc1.proyecto1.logica.Animal;
import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.Parcela;
import com.ipc1.proyecto1.logica.SueloGrama;
import com.ipc1.proyecto1.logica.TipoAnimal;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class MercadoAnimalesGUI extends javax.swing.JFrame {

    private GranjaGUI ventanaGranja;
    private CasillaGUI casillaGUI;

    public MercadoAnimalesGUI(CasillaGUI casillaGUI, GranjaGUI ventanaGranja) {
        this.ventanaGranja = ventanaGranja;
        this.casillaGUI = casillaGUI;
        initComponents();
        iniciarComponentes();
        listarAnimales();
    }

    public void iniciarComponentes() {
        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    panelAnimales.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Parcela " + parcela.getNumeroParcela(), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Dialog", 0, 11), new java.awt.Color(255, 255, 255))); // NOI18N
                }
            }
        }

        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    if (parcela.getAnimales() != null) {
                        for (Animal animale : parcela.getAnimales()) {
                            comboAnimalesParcela.addItem(animale.getNombreAnimal());
                            datosAnimalVivo();
                        }
                    }
                }
            }

        }

    }

    private void listarAnimales() {
        for (Animal cria : ventanaGranja.getMercado().getCrias()) {
            comboAnimales.addItem(cria.getNombreAnimal());
        }
    }

    public void verPrecioAnimal() {
        for (Animal cria : ventanaGranja.getMercado().getCrias()) {
            if (comboAnimales.getSelectedItem().toString().equals(cria.getNombreAnimal())) {
                etiquetaDatosAnimal.setText("Precio:  Q" + cria.getPrecio());
            }
        }
    }

    public void mostrarDatosAnimal() {

        for (Animal cria : ventanaGranja.getMercado().getCrias()) {
            if (comboAnimalesParcela.getItemCount() > 0) {
                try {
                    if (comboAnimalesParcela.getSelectedItem().toString().equals(cria.getNombreAnimal())) {
                        etiquetaMostrarDatosAnimalConD.setText(cria.verDatosDestaceAnimal());
                        etiquetaMostrarDatosAnimalSinD.setText(cria.verDatosSinDestace());
                    }
                } catch (NullPointerException e) {

                }
            }
        }
    }

    public void datosAnimalVivo() {
        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    for (Animal animale : parcela.getAnimales()) {
                        if (comboAnimalesParcela.getSelectedItem().toString().equals(animale.getNombreAnimal())) {
                            etiquetaVidaAnimal.setText("" + animale.getVida());
                        }
                    }
                }
            }
        }

    }

    public Animal crearAnimal(int indice) {
        Animal animal = ventanaGranja.getMercado().getCrias()[indice];
        Animal cria = new Animal(animal.getNombreAnimal(), animal.getVida(), animal.getEspacioOcupado(), animal.getComidaDestace(),
                animal.getComidaSinDestace(), animal.getMateriaPrimaConDestace(), animal.getMateriaPrimaSinDestace(), animal.getPrecio(),
                animal.getTipoAnimal(), animal.getEdad());

        return cria;
    }

    public void comprarAnimal() {
        int indice = comboAnimales.getSelectedIndex();
        Animal animal = crearAnimal(indice);
        for (int i = 0; i < ventanaGranja.getGranja().getParcelas().length; i++) {
            for (int j = 0; j < ventanaGranja.getGranja().getParcelas()[i].getSuelosGrama().length; j++) {
                if (casillaGUI.getNumeroCasilla() == ventanaGranja.getGranja().getParcelas()[i].getSuelosGrama()[j].getNumero()) {
                    if (animal.getEspacioOcupado() <= ventanaGranja.getGranja().getParcelas()[i].getEspacioParcela() && ventanaGranja.getGranjero().getOro() > animal.getPrecio()) {
                        ventanaGranja.getGranja().getParcelas()[i].agregarAnimal(animal);
                        JOptionPane.showMessageDialog(null, "Se ha comprado un(a) " + animal.getNombreAnimal());
                        ventanaGranja.getGranja().getParcelas()[i].setEspacioParcela(ventanaGranja.getGranja().getParcelas()[i].getEspacioParcela() - animal.getEspacioOcupado());
                        ventanaGranja.getGranjero().gastarOro(animal.getPrecio());
                        ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                        comboAnimalesParcela.addItem(animal.getNombreAnimal());
                        ventanaGranja.getAnimalesComprados()[indice]++;
                        if (animal.getNombreAnimal().equals("Vaca")) {
                            casillaGUI.setBackgroundImage(ImagesEnum.VACA);
                        } else if (animal.getNombreAnimal().equals("Gallina")) {
                            casillaGUI.setBackgroundImage(ImagesEnum.GALLINA);
                        }
                        animal.iniciarVidaAnimal(casillaGUI);
                    } else {
                        JOptionPane.showMessageDialog(null, "No se pudo comprar el Animal");
                    }

                }
            }
        }

    }

    public void destazarAnimal() {
        int indice = comboAnimalesParcela.getSelectedIndex();
        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    try {
                        Animal animal = parcela.getAnimales()[indice];
                        if (animal.getComidaDestace() != 0 && !animal.murioDeViejo() && !animal.estaMuerto()) {
                            parcela.liberarEspacioParcela(animal.getEspacioOcupado());
                            ventanaGranja.getGranja().entrarBodega().recogerAlimentoAnimal(animal.getComidaDestace());
                            ventanaGranja.getGranja().entrarBodega().recogerMateriaPrima(animal.getMateriaPrimaConDestace());
                            ventanaGranja.getBodegaGUI().getEtiquetaAlimentoProducidoAnimal().setText("" + ventanaGranja.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                            ventanaGranja.getMercadoGUI().getEtiquetaAlimentoProducidoAnimales().setText("" + ventanaGranja.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                            ventanaGranja.getBodegaGUI().getEtiquetaMateriaProducida().setText("" + ventanaGranja.getGranja().entrarBodega().getMateriaPrimaRecogida());
                            ventanaGranja.getMercadoGUI().getEtiquetaMateriaPrima().setText("" + ventanaGranja.getGranja().entrarBodega().getMateriaPrimaRecogida());
                            JOptionPane.showMessageDialog(null, "Destazamos un(a) " + animal.getNombreAnimal());
                            GranjaGUI.animalesDestazados++;
                            Animal animales[] = parcela.eliminarAnimal(indice);
                            parcela.setAnimales(animales);
                            animal.fueDestazado(true);
                            GranjaGUI.alimentoGenerado += animal.getComidaDestace();
                            comboAnimalesParcela.removeItemAt(indice);
                            casillaGUI.setBackgroundImage(ImagesEnum.PARCELA);
                        } else {
                            JOptionPane.showMessageDialog(null, animal.getNombreAnimal() + " no puede producir alimento");
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        JOptionPane.showMessageDialog(null, "No hay ningun animal para destazar");
                    }

                }
            }
        }
    }

    public void recogerAlimentoSinDestazar() {
        int indice = comboAnimalesParcela.getSelectedIndex();
        for (int i = 0; i < ventanaGranja.getGranja().getParcelas().length; i++) {
            for (int j = 0; j < ventanaGranja.getGranja().getParcelas()[i].getSuelosGrama().length; j++) {
                if (casillaGUI.getNumeroCasilla() == ventanaGranja.getGranja().getParcelas()[i].getSuelosGrama()[j].getNumero()) {
                    try {
                        Animal animal = ventanaGranja.getGranja().getParcelas()[i].getAnimales()[indice];
                        if (animal.getComidaSinDestace() != 0 && !animal.estaMuerto() && !animal.murioDeViejo()) {
                            ventanaGranja.getGranja().entrarBodega().recogerAlimentoAnimal(animal.getComidaSinDestace());
                            ventanaGranja.getBodegaGUI().getEtiquetaAlimentoProducidoAnimal().setText("" + ventanaGranja.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                            ventanaGranja.getMercadoGUI().getEtiquetaAlimentoProducidoAnimales().setText("" + ventanaGranja.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                            GranjaGUI.alimentoGenerado += animal.getComidaSinDestace();
                        } else {
                            JOptionPane.showMessageDialog(null, animal.getNombreAnimal() + " no puede producir alimento");
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        JOptionPane.showMessageDialog(null, "No hay ningún animal para generar alimento");
                    }
                }
            }
        }

    }

    public void recogerMateriaPrimaSinDestace() {
        int indice = comboAnimalesParcela.getSelectedIndex();
        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    try {
                        Animal animal = parcela.getAnimales()[indice];
                        if (animal.getMateriaPrimaSinDestace() != 0 && !animal.estaMuerto() && !animal.murioDeViejo()) {
                            ventanaGranja.getGranja().entrarBodega().recogerMateriaPrima(animal.getMateriaPrimaSinDestace());
                            ventanaGranja.getBodegaGUI().getEtiquetaMateriaProducida().setText("" + ventanaGranja.getGranja().entrarBodega().getMateriaPrimaRecogida());
                            ventanaGranja.getMercadoGUI().getEtiquetaMateriaPrima().setText("" + ventanaGranja.getGranja().entrarBodega().getMateriaPrimaRecogida());
                        } else {
                            JOptionPane.showMessageDialog(null, animal.getNombreAnimal() + " no puede producir alimento");
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        JOptionPane.showMessageDialog(null, "No hay ningún animal para generar alimento");
                    }
                }
            }
        }

    }

    public void eliminarAnimal() {

        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() != suelosGrama.getNumero()) {
                    continue;
                }
                int indice = comboAnimalesParcela.getSelectedIndex();
                for (int k = 0; k < parcela.getAnimales().length; k++) {
                    if (k == indice) {
                        Animal animal = parcela.getAnimales()[indice];
                        if (animal.murioDeViejo()) {
                            Animal animales[] = parcela.eliminarAnimal(indice);
                            parcela.setAnimales(animales);
                            comboAnimalesParcela.removeItemAt(indice);
                            casillaGUI.setBackgroundImage(ImagesEnum.PARCELA);
                            JOptionPane.showMessageDialog(null, "Se eliminó un animal");
                        } else {
                            JOptionPane.showMessageDialog(null, "El animal sigue vivo, no se puede eliminar");
                        }

                    }
                }
            }
        }

    }

    public void actualizarArray() {

        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    for (Animal animale : parcela.getAnimales()) {
                        comboAnimalesParcela.addItem(animale.getNombreAnimal());
                    }
                }
            }
        }

    }

    public void alimentarAnimal() {
        int indice = comboAnimalesParcela.getSelectedIndex();
        for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
            for (SueloGrama suelosGrama : parcela.getSuelosGrama()) {
                if (casillaGUI.getNumeroCasilla() == suelosGrama.getNumero()) {
                    try {
                        Animal animal = parcela.getAnimales()[indice];
                        if (!animal.estaMuerto() && !animal.murioDeViejo()) {
                            if (animal.getTipoAnimal() == TipoAnimal.HERVIBORO) {
                                alimentarAnimalHerbivoro(animal);
                            } else if (animal.getTipoAnimal() == TipoAnimal.OMNIVORO) {
                                alimentarAnimalOmnivoro(animal);
                            }
                        }
                    } catch (ArrayIndexOutOfBoundsException e) {
                        JOptionPane.showMessageDialog(null, "No hay ningún animal para alimentarlo");
                    }
                }
            }
        }
    }

    public void alimentarAnimalHerbivoro(Animal animal) {
        switch (comboAlimentoAnimal.getSelectedIndex()) {
            case 0:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalHerbiboro.PASTO.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalHerbiboro.PASTO.getAlimentoHerbiboro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalHerbiboro.PASTO.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }

                break;
            case 1:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalHerbiboro.HIERBA.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalHerbiboro.HIERBA.getAlimentoHerbiboro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalHerbiboro.HIERBA.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }

                break;
            case 2:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalHerbiboro.VEGETALES.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalHerbiboro.VEGETALES.getAlimentoHerbiboro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalHerbiboro.VEGETALES.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }

                break;
            default:
                JOptionPane.showMessageDialog(null, "Este es un animal Herbivoro");
                break;
        }

    }

    public void alimentarAnimalOmnivoro(Animal animal) {
        switch (comboAlimentoAnimal.getSelectedIndex()) {
            case 3:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalOmnivoro.CARNE.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalOmnivoro.CARNE.getAlimentoOmnivoro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalOmnivoro.CARNE.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }

                break;
            case 4:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalOmnivoro.CARNE.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalOmnivoro.FRUTAS.getAlimentoOmnivoro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalOmnivoro.FRUTAS.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }

                break;
            case 5:
                if (ventanaGranja.getGranjero().getOro() >= AlimentoAnimalOmnivoro.CARNE.getPrecioAlimento()) {
                    animal.alimentarAnimal(AlimentoAnimalOmnivoro.VERDURAS.getAlimentoOmnivoro());
                    ventanaGranja.getGranjero().gastarOro(AlimentoAnimalOmnivoro.VERDURAS.getPrecioAlimento());
                    ventanaGranja.getEtiquetaOroGranjero().setText("" + ventanaGranja.getGranjero().getOro());
                } else {
                    JOptionPane.showMessageDialog(null, "No tienes suficientes monedas de oro");
                }
                break;
            default:
                JOptionPane.showMessageDialog(null, "Este es un animal Omnivoro");
                break;
        }
    }

    public JPanel getPanelAnimales() {
        return panelAnimales;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelAnimales = new javax.swing.JPanel();
        etiquetaMercadoAnimales = new javax.swing.JLabel();
        etiquetaCria = new javax.swing.JLabel();
        comboAnimales = new javax.swing.JComboBox<>();
        botonComprarAnimal = new javax.swing.JButton();
        etiquetaDatosAnimal = new javax.swing.JLabel();
        comboAnimalesParcela = new javax.swing.JComboBox<>();
        jLabel1 = new javax.swing.JLabel();
        botonDestazar = new javax.swing.JButton();
        botonRecogerSinDestazar = new javax.swing.JButton();
        botonEliminatAnimal = new javax.swing.JButton();
        etiquetaMostrarDatosAnimalConD = new javax.swing.JLabel();
        etiquetaMostrarDatosAnimalSinD = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        etiquetaVidaAnimal = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        etiquetaEdadAnimal = new javax.swing.JLabel();
        botonMateriaPrimaSinDestace = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        comboAlimentoAnimal = new javax.swing.JComboBox<>();
        botonAlimentarAnimal = new javax.swing.JButton();

        setResizable(false);

        panelAnimales.setBackground(new java.awt.Color(51, 51, 51));
        panelAnimales.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        etiquetaMercadoAnimales.setFont(new java.awt.Font("Dubai", 1, 24)); // NOI18N
        etiquetaMercadoAnimales.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaMercadoAnimales.setText("Mercado de Animales");

        etiquetaCria.setFont(new java.awt.Font("Dubai", 1, 18)); // NOI18N
        etiquetaCria.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaCria.setText("Cría ");

        comboAnimales.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAnimalesActionPerformed(evt);
            }
        });

        botonComprarAnimal.setFont(new java.awt.Font("Dialog", 1, 11)); // NOI18N
        botonComprarAnimal.setText("COMPRAR CRÍA");
        botonComprarAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComprarAnimalActionPerformed(evt);
            }
        });

        etiquetaDatosAnimal.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaDatosAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        comboAnimalesParcela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboAnimalesParcelaActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Animales en Parcela");

        botonDestazar.setText("Destazar");
        botonDestazar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonDestazarActionPerformed(evt);
            }
        });

        botonRecogerSinDestazar.setText("Recoger Alimento Sin Destazar");
        botonRecogerSinDestazar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRecogerSinDestazarActionPerformed(evt);
            }
        });

        botonEliminatAnimal.setText("ELIMINAR ANIMAL MUERTO");
        botonEliminatAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminatAnimalActionPerformed(evt);
            }
        });

        etiquetaMostrarDatosAnimalConD.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaMostrarDatosAnimalConD.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        etiquetaMostrarDatosAnimalSinD.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaMostrarDatosAnimalSinD.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Vida:");

        etiquetaVidaAnimal.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaVidaAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel4.setText("Edad:");

        etiquetaEdadAnimal.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        botonMateriaPrimaSinDestace.setText("Recoger materia prima sin destazar");
        botonMateriaPrimaSinDestace.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonMateriaPrimaSinDestaceActionPerformed(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel3.setFont(new java.awt.Font("Dubai", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("ALIMENTO ANIMAL");

        comboAlimentoAnimal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "PASTO  Q5  +15 vida  HERBIVORO", "HIERBA Q10 +25 vida  HERBIVORO", "VEGETALES Q15 +35 vida  HERBIVORO", "CARNE  Q5  +15 vida OMNIVORO", "FRUTAS  Q10 +25 vida OMNIVORO", "VERDURAS  Q15 +35 vida OMNIVORO", " " }));

        botonAlimentarAnimal.setText("ALIMENTAR ANIMAL");
        botonAlimentarAnimal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAlimentarAnimalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(119, 119, 119)
                .addComponent(jLabel3)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(39, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(comboAlimentoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(37, 37, 37))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(botonAlimentarAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(57, 57, 57))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(45, 45, 45)
                .addComponent(comboAlimentoAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(botonAlimentarAnimal)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelAnimalesLayout = new javax.swing.GroupLayout(panelAnimales);
        panelAnimales.setLayout(panelAnimalesLayout);
        panelAnimalesLayout.setHorizontalGroup(
            panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAnimalesLayout.createSequentialGroup()
                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(etiquetaMostrarDatosAnimalSinD, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(etiquetaMostrarDatosAnimalConD, javax.swing.GroupLayout.PREFERRED_SIZE, 285, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(botonEliminatAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 213, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(panelAnimalesLayout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(etiquetaVidaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(18, 18, 18)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(etiquetaEdadAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(87, 87, 87)
                        .addComponent(botonComprarAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 209, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(128, 128, 128)
                        .addComponent(etiquetaDatosAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(53, 53, 53)
                        .addComponent(comboAnimalesParcela, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(117, 117, 117)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(142, 142, 142)
                        .addComponent(etiquetaCria))
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addGap(70, 70, 70)
                        .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(etiquetaMercadoAnimales))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 27, Short.MAX_VALUE)
                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAnimalesLayout.createSequentialGroup()
                        .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonMateriaPrimaSinDestace, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonRecogerSinDestazar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonDestazar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAnimalesLayout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        panelAnimalesLayout.setVerticalGroup(
            panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelAnimalesLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(etiquetaMercadoAnimales)
                .addGap(18, 18, 18)
                .addComponent(etiquetaCria, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addComponent(comboAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(etiquetaDatosAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botonComprarAnimal)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(comboAnimalesParcela, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(etiquetaMostrarDatosAnimalConD, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelAnimalesLayout.createSequentialGroup()
                        .addComponent(etiquetaMostrarDatosAnimalSinD, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panelAnimalesLayout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel2)
                                    .addComponent(etiquetaVidaAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(panelAnimalesLayout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addGroup(panelAnimalesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(etiquetaEdadAnimal, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel4))))
                        .addGap(44, 44, 44)
                        .addComponent(botonEliminatAnimal)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelAnimalesLayout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 20, Short.MAX_VALUE)
                        .addComponent(botonMateriaPrimaSinDestace)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonRecogerSinDestazar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(botonDestazar)
                        .addGap(61, 61, 61))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAnimales, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelAnimales, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComprarAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComprarAnimalActionPerformed
        comprarAnimal();


    }//GEN-LAST:event_botonComprarAnimalActionPerformed

    private void comboAnimalesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAnimalesActionPerformed
        verPrecioAnimal();

    }//GEN-LAST:event_comboAnimalesActionPerformed

    private void comboAnimalesParcelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboAnimalesParcelaActionPerformed
        if (ventanaGranja.getGranja().getParcelas() != null) {
            for (Parcela parcela : ventanaGranja.getGranja().getParcelas()) {
                if (parcela.getAnimales() != null) {
                    mostrarDatosAnimal();
                    datosAnimalVivo();
                }
            }
        }


    }//GEN-LAST:event_comboAnimalesParcelaActionPerformed

    private void botonDestazarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonDestazarActionPerformed

        destazarAnimal();
    }//GEN-LAST:event_botonDestazarActionPerformed

    private void botonRecogerSinDestazarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRecogerSinDestazarActionPerformed

        recogerAlimentoSinDestazar();
    }//GEN-LAST:event_botonRecogerSinDestazarActionPerformed

    private void botonEliminatAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminatAnimalActionPerformed
        eliminarAnimal();
        //actualizarArray();
    }//GEN-LAST:event_botonEliminatAnimalActionPerformed

    private void botonMateriaPrimaSinDestaceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonMateriaPrimaSinDestaceActionPerformed
        recogerMateriaPrimaSinDestace();
    }//GEN-LAST:event_botonMateriaPrimaSinDestaceActionPerformed

    private void botonAlimentarAnimalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAlimentarAnimalActionPerformed
        alimentarAnimal();

    }//GEN-LAST:event_botonAlimentarAnimalActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAlimentarAnimal;
    private javax.swing.JButton botonComprarAnimal;
    private javax.swing.JButton botonDestazar;
    private javax.swing.JButton botonEliminatAnimal;
    private javax.swing.JButton botonMateriaPrimaSinDestace;
    private javax.swing.JButton botonRecogerSinDestazar;
    private javax.swing.JComboBox<String> comboAlimentoAnimal;
    private javax.swing.JComboBox<String> comboAnimales;
    private javax.swing.JComboBox<String> comboAnimalesParcela;
    private javax.swing.JLabel etiquetaCria;
    private javax.swing.JLabel etiquetaDatosAnimal;
    private javax.swing.JLabel etiquetaEdadAnimal;
    private javax.swing.JLabel etiquetaMercadoAnimales;
    private javax.swing.JLabel etiquetaMostrarDatosAnimalConD;
    private javax.swing.JLabel etiquetaMostrarDatosAnimalSinD;
    private javax.swing.JLabel etiquetaVidaAnimal;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel panelAnimales;
    // End of variables declaration//GEN-END:variables
}
