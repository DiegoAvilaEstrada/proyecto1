package com.ipc1.proyecto1.gui;

import com.ipc1.proyecto1.logica.Granja;
import com.ipc1.proyecto1.logica.ImagesEnum;
import com.ipc1.proyecto1.logica.Parcela;
import com.ipc1.proyecto1.logica.SueloGrama;
import javax.swing.JOptionPane;

public class AdquirirParcelaGUI extends javax.swing.JFrame {

    private Parcela parcela;
    private final GranjaGUI granjaGUI;

    public void añadirSueloGrama() {
        if (parcela == null) {
            parcela = new Parcela(granjaGUI.getGranja().getParcelas().length + 1);
        }
        for (int fila = 0; fila < Granja.NUMERO_MAXIMO_FILAS; fila++) {
            for (int columna = 0; columna < Granja.NUMERO_MAXIMO_COLUMNAS; columna++) {
                if (Integer.parseInt(numeroElegido.getText()) != granjaGUI.getCasilla(fila, columna).getNumeroCasilla()) {
                    continue;
                }
                if (!(granjaGUI.getCasilla(fila, columna).getSuelo() instanceof SueloGrama)) {
                    JOptionPane.showMessageDialog(null, "Solo los suelos grama pueden añadirse a una parcela");
                    return;
                }
                SueloGrama sueloGrama = (SueloGrama) granjaGUI.getCasilla(fila, columna).getSuelo();
                if (!granjaGUI.getGranja().existeEnOtraParcela(sueloGrama)) {
                    boolean añadidoAParcela = parcela.agregarSueloGrama(sueloGrama);
                    if (añadidoAParcela == false) {
                        JOptionPane.showMessageDialog(null, "No se ha podido añadir");
                        return;
                    }
                    System.out.println(granjaGUI.getCasilla(fila, columna).getNumeroCasilla() + ": " + añadidoAParcela);
                    granjaGUI.getCasilla(fila, columna).setBackgroundImage(ImagesEnum.PARCELA);
                } else {
                    JOptionPane.showMessageDialog(null, "La casilla " + numeroElegido.getText() + " ya existe en otra parcela!");
                }
            }
        }
    }

    public AdquirirParcelaGUI(GranjaGUI granjaGUI) {
        this.granjaGUI = granjaGUI;
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        numeroElegido = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        botonAñadirParcela = new javax.swing.JButton();
        botonFinalizar = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(51, 51, 51));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("CREAR PARCELA");

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("No. de Casilla");

        botonAñadirParcela.setText("Añadir a Parcela");
        botonAñadirParcela.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAñadirParcelaActionPerformed(evt);
            }
        });

        botonFinalizar.setText("Crear Parcela");
        botonFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFinalizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 87, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(49, 49, 49)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(143, 143, 143)
                        .addComponent(numeroElegido, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(121, 121, 121)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(botonAñadirParcela, javax.swing.GroupLayout.DEFAULT_SIZE, 159, Short.MAX_VALUE)
                            .addComponent(botonFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(numeroElegido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(botonAñadirParcela)
                .addGap(18, 18, 18)
                .addComponent(botonFinalizar)
                .addContainerGap(45, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAñadirParcelaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAñadirParcelaActionPerformed
        try {
            añadirSueloGrama();
            numeroElegido.setText("");
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "No has añadido ningun suelo grama a Parcela");
        }


    }//GEN-LAST:event_botonAñadirParcelaActionPerformed

    private void botonFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFinalizarActionPerformed
        if (parcela != null && parcela.getSuelosGrama().length > 0) {
            this.granjaGUI.getGranja().agrandarArregloYAñadir(parcela);
        }
        parcela.setEspacioParcela(parcela.getSuelosGrama().length);
        this.setVisible(false);
    }//GEN-LAST:event_botonFinalizarActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAñadirParcela;
    private javax.swing.JButton botonFinalizar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField numeroElegido;
    // End of variables declaration//GEN-END:variables
}
