package com.ipc1.proyecto1.gui;

import java.awt.Image;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

public class MercadoGUI extends javax.swing.JFrame {

    GranjaGUI parent;

    public static int oroGenerado = 0;

    public MercadoGUI(GranjaGUI parent) {
        this.parent = parent;
        initComponents();

    }

    public JLabel getEtiquetaAlimentoProducidoAnimales() {
        return etiquetaAlimentoProducidoAnimales;
    }

    public JLabel getEtiquetaAlimentoProducidoPlantas() {
        return etiquetaAlimentoProducidoPlantas;
    }

    public JLabel getEtiquetaMateriaPrima() {
        return etiquetaMateriaPrima;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panelMercado = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        etiquetaAlimentoProducidoAnimales = new javax.swing.JLabel();
        etiquetaAlimentoProducidoPlantas = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        etiquetaMateriaPrima = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        comboProductos = new javax.swing.JComboBox<>();
        botonVenderProductos = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();

        panelMercado.setBackground(new java.awt.Color(51, 51, 51));

        jPanel2.setBackground(new java.awt.Color(51, 51, 51));
        jPanel2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel2.setFont(new java.awt.Font("Dubai", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Alimento producido por plantas:");

        jLabel4.setFont(new java.awt.Font("Dubai", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Alimento producido por animales:");

        etiquetaAlimentoProducidoAnimales.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoProducidoAnimales.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        etiquetaAlimentoProducidoPlantas.setForeground(new java.awt.Color(255, 255, 255));
        etiquetaAlimentoProducidoPlantas.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel6.setForeground(new java.awt.Color(255, 255, 255));
        jLabel6.setText("Materia Prima");

        etiquetaMateriaPrima.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jLabel1.setFont(new java.awt.Font("Dubai", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("MIS PRODUCTOS");

        comboProductos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Alimento producido por plantas", "Alimento producido por animales", "Materia Prima" }));
        comboProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboProductosActionPerformed(evt);
            }
        });

        botonVenderProductos.setBackground(new java.awt.Color(51, 51, 51));
        botonVenderProductos.setForeground(new java.awt.Color(255, 255, 255));
        botonVenderProductos.setText("VENDER  PRODUCTO");
        botonVenderProductos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonVenderProductosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addGap(0, 203, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(201, 201, 201))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(botonVenderProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(70, 70, 70))))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, 201, Short.MAX_VALUE)
                                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(33, 33, 33)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(etiquetaAlimentoProducidoAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(etiquetaAlimentoProducidoPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(etiquetaMateriaPrima, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(comboProductos, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap())))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(etiquetaAlimentoProducidoPlantas, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel4)
                    .addComponent(etiquetaAlimentoProducidoAnimales, javax.swing.GroupLayout.PREFERRED_SIZE, 17, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(comboProductos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(etiquetaMateriaPrima, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(botonVenderProductos)
                .addGap(21, 21, 21))
        );

        jLabel8.setFont(new java.awt.Font("Dubai", 0, 28)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 255, 255));
        jLabel8.setText("M E R C A D O");

        javax.swing.GroupLayout panelMercadoLayout = new javax.swing.GroupLayout(panelMercado);
        panelMercado.setLayout(panelMercadoLayout);
        panelMercadoLayout.setHorizontalGroup(
            panelMercadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMercadoLayout.createSequentialGroup()
                .addGap(41, 41, 41)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(46, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMercadoLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(222, 222, 222))
        );
        panelMercadoLayout.setVerticalGroup(
            panelMercadoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMercadoLayout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMercado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelMercado, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonVenderProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonVenderProductosActionPerformed
        switch (comboProductos.getSelectedIndex()) {
            case 0:
                if (parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta() > 0) {
                    parent.getGranja().entrarBodega().retirarAlimentoPlanta(1);
                    parent.getGranjero().ganarOro(10);
                    oroGenerado += 10;
                    parent.getEtiquetaOroGranjero().setText("" + parent.getGranjero().getOro());
                    parent.getBodegaGUI().getEtiquetaAlimentoProducidoPlanta().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta());
                    etiquetaAlimentoProducidoPlantas.setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoPlanta());
                } else {
                    JOptionPane.showMessageDialog(null, "No hay más alimento de plantas");
                }
                break;
            case 1:
                if (parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal() > 0) {
                    parent.getGranja().entrarBodega().retirarAlimentoAnimal(1);
                    parent.getGranjero().ganarOro(15);
                    oroGenerado += 15;
                    parent.getEtiquetaOroGranjero().setText("" + parent.getGranjero().getOro());
                    parent.getBodegaGUI().getEtiquetaAlimentoProducidoAnimal().setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                    etiquetaAlimentoProducidoAnimales.setText("" + parent.getGranja().entrarBodega().getAlimentoRecogidoAnimal());
                } else {
                    JOptionPane.showMessageDialog(null, "No hay más alimento de Animales");
                }
                break;
            case 2:
                if (parent.getGranja().entrarBodega().getMateriaPrimaRecogida() > 0) {
                    parent.getGranja().entrarBodega().recogerMateriaPrima(1);
                    parent.getGranjero().ganarOro(20);
                    oroGenerado += 20;
                    parent.getEtiquetaOroGranjero().setText("" + parent.getGranjero().getOro());
                    parent.getBodegaGUI().getEtiquetaMateriaProducida().setText("" + parent.getGranja().entrarBodega().getMateriaPrimaRecogida());
                    etiquetaMateriaPrima.setText("" + parent.getGranja().entrarBodega().getMateriaPrimaRecogida());
                } else {
                    JOptionPane.showMessageDialog(null, "No hay más materia prima");
                }
                break;
            default:
                break;
        }
    }//GEN-LAST:event_botonVenderProductosActionPerformed

    private void comboProductosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboProductosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_comboProductosActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonVenderProductos;
    private javax.swing.JComboBox<String> comboProductos;
    private javax.swing.JLabel etiquetaAlimentoProducidoAnimales;
    private javax.swing.JLabel etiquetaAlimentoProducidoPlantas;
    private javax.swing.JLabel etiquetaMateriaPrima;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel panelMercado;
    // End of variables declaration//GEN-END:variables
}
